package com.raz.razht.razielmap.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.raz.razht.razielmap.db.DBConstants;

@Entity(tableName = "table_places")
public class PlaceRoom implements Parcelable {

    public static final Parcelable.Creator<PlaceRoom> CREATOR = new Parcelable.Creator<PlaceRoom>() {
        public PlaceRoom createFromParcel(Parcel in) {
            return new PlaceRoom( in );
        }

        public PlaceRoom[] newArray(int size) {
            return new PlaceRoom[size];
        }
    };
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_API_ID)
    private String apiId;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_NAME)
    private String locationName;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_ADDRESS)
    private String address;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_ICON)
    private String icon;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_IMAGE)
    private String imagePlace;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_RATING)
    private double rating;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE)
    private String type;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE)
    private String lang;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE)
    private String lat;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE)
    private String distance;
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_LOCATION_IS_OPEN)
    private int isOpen;

    @NonNull
    public int getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(@NonNull int isFavorite) {
        this.isFavorite = isFavorite;
    }

    //1 = favorite, 0= no
    @NonNull
    @ColumnInfo(name = DBConstants.COLUMN_NAME_IS_FAVORITE)
    private int isFavorite;


    public PlaceRoom() {
    }

    public PlaceRoom(@NonNull String apiId, @NonNull String locationName, @NonNull String address, @NonNull String distance, @NonNull String type) {
        this.apiId = apiId;
        this.locationName = locationName;
        this.address = address;
        this.distance = distance;
        this.type = type;
    }

    public PlaceRoom(@NonNull String apiId, @NonNull String locationName, @NonNull String address, @NonNull String icon, @NonNull String imagePlace, @NonNull double rating,
                     @NonNull String type, @NonNull String lang, @NonNull String lat, @NonNull String distance, @NonNull int isOpen) {

        this.apiId = apiId;
        this.locationName = locationName;
        this.address = address;
        this.icon = icon;
        this.imagePlace = imagePlace;
        this.rating = rating;
        this.type = type;
        this.lang = lang;
        this.lat = lat;
        this.distance = distance;
        this.isOpen = isOpen;
    }

    public PlaceRoom(@NonNull int id, @NonNull String apiId, @NonNull String locationName, @NonNull String address,
                     @NonNull String icon, @NonNull String imagePlace, @NonNull double rating, @NonNull String type, @NonNull String lang, @NonNull String lat, @NonNull String distance, @NonNull int isOpen) {
        this.id = id;
        this.apiId = apiId;
        this.locationName = locationName;
        this.address = address;
        this.icon = icon;
        this.imagePlace = imagePlace;
        this.rating = rating;
        this.type = type;
        this.lang = lang;
        this.lat = lat;
        this.distance = distance;
        this.isOpen = isOpen;

    }

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private PlaceRoom(@NonNull Parcel in) {
        id = in.readInt();
        apiId = in.readString();
        locationName = in.readString();
        address = in.readString();
        icon = in.readString();
        imagePlace = in.readString();
        rating = in.readDouble();
        type = in.readString();
        lang = in.readString();
        lat = in.readString();
        distance = in.readString();
        isOpen = in.readInt();
    }

    public int getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(int isOpen) {
        this.isOpen = isOpen;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getImagePlace() {
        return imagePlace;
    }

    public void setImagePlace(String imagePlace) {
        this.imagePlace = imagePlace;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt( id );
        out.writeString( apiId );
        out.writeString( locationName );
        out.writeString( address );
        out.writeString( icon );
        out.writeString( imagePlace );
        out.writeDouble( rating );
        out.writeString( type );
        out.writeString( lang );
        out.writeString( lat );
        out.writeString( distance );
        out.writeInt( isOpen );
    }
}


