package com.raz.razht.razielmap.ui.place;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.GlideApp;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.Service.GoogleApiService;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.db.FileHelper;
import com.raz.razht.razielmap.db.Place;
import com.raz.razht.razielmap.ui.place.adapter.PhotosPlaceAdapter;
import com.raz.razht.razielmap.ui.streetview.StreetViewActivity;
import com.raz.razht.razielmap.util.Constants;
import com.raz.razht.razielmap.util.DownloadUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaceActivity extends AppCompatActivity implements View.OnClickListener, OnMapAndViewReadyListener.OnGlobalLayoutAndMapReadyListener {

    @BindView(R.id.imageViewPlace)
    ImageView imageViewPlace;
    @BindView(R.id.ratingPlace)
    RatingBar ratingPlace;
    @BindView(R.id.tvDistance)
    TextView tvDistance;
    @BindView(R.id.tvCountry)
    TextView tvCountry;
    @BindView(R.id.tvPlaceName)
    TextView tvPlaceName;
    @BindView(R.id.typePlace)
    TextView typePlace;
   /* @BindView( R.id.toolBar )
    Toolbar toolbar;*/

    @BindView(R.id.shareBtn)
    ImageButton shareBtn;
    @BindView(R.id.saveToFavoriteBtn)
    ImageButton saveToFavoriteBtn;

    @BindView(R.id.navigateBtn)
    ImageButton navigateBtn;
    @BindView(R.id.streetView)
    ImageView streetView;
    @BindView(R.id.locationDetails)
    TextView locationDetails;
    @BindView(R.id.rowImages)
    RecyclerView rowImages;
    @BindView(R.id.isOpen)
    TextView isOpen;
    App appReference;
    CoordinatorLayout coordinatorLayout;
    Intent intent;
    private GoogleMap mMap;
    private Place place;
    private ServiceFinishedBroadcastReceiver receiver;
    private ArrayList<String> imagesArray;
    private RecyclerView mImageRecyclerView;
    private PhotosPlaceAdapter mPhotosPlaceAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private FileHelper fileHelper;

    public static boolean isValidContextForGlide(final Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            if (activity.isDestroyed() || activity.isFinishing()) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appReference = (App) getApplication();
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_place );
        ButterKnife.bind( this );
        coordinatorLayout = (CoordinatorLayout) findViewById( R.id.activity_place );
        fileHelper = new FileHelper( this );
        intent = getIntent();
        setDataIntent( intent );
        setFavoriteBtnColor();
        streetView.setOnClickListener( this );
        saveToFavoriteBtn.setOnClickListener( this );
        navigateBtn.setOnClickListener( this );
        shareBtn.setOnClickListener( this );
        createIntentFilter();
        SupportMapFragment mapFragment =
                (SupportMapFragment) this.getSupportFragmentManager().findFragmentById( R.id.liteMap );
        new OnMapAndViewReadyListener( mapFragment, this );


    }

    public void showPlace(Place place) {

        // Wait until map is ready
        if (mMap == null) {
            return;
        }

        // Center camera on Place marker
        try {
            LatLng latLng = new LatLng( Double.parseDouble( place.getLat() ), Double.parseDouble( place.getLang() ) );
            mMap.moveCamera( CameraUpdateFactory.newLatLngZoom( latLng, 15f ) );
            mMap.addMarker( new MarkerOptions()
                    .position( latLng )
                    .title( place.getLocationName() ) );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setDataIntent(Intent intent) {
        if (intent.getBooleanExtra( "AUTO_COMPLETE", false )) {
            sendPlaceIdQuery( intent.getStringExtra( "PLACE_ID" ) );
            sendLiveDataQuery( intent.getStringExtra( "PLACE_ID" ) );
        } else if (intent.hasExtra( "PLACE_ID" )) {
            String place_id = intent.getStringExtra( "PLACE_ID" );
            Place place = appReference.getHandler().selectPlaceByPlaceApiId( place_id + "", appReference.getPreferenceHelper().getTableName() );
            setData( place );
            sendLiveDataQuery( place.getApiId() );
        } else if (intent.hasExtra( "PLACE" )) {
            Place placeFromFavorite = intent.getParcelableExtra( "PLACE" );
            this.place = placeFromFavorite;
            sendLiveDataQuery( place.getApiId() );
            setData( place );
        }

    }


    public void setData(Place place) {
        this.place = place;
        if (place.getImagePlace().contains( "/data/user" )) {
            fileHelper.loadImageFromStorage( place.getImagePlace(), imageViewPlace );
        } else if (place.getImagePlace().length() > 10) {
            if (isValidContextForGlide( this )) {
                try {
                    GlideApp.with( getApplicationContext() ).load( Constants.sendImageRequest( place.getImagePlace(), getResources().getString( R.string.google_maps_key ) ) ).into( imageViewPlace );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        String unit = appReference.getPreferenceHelper().getPreferenceUnitsApp();
        String distance = DownloadUrl.CalculationByDistance( appReference.getPreferenceHelper().getUserPreferenceLanLat(), new LatLng( Double.valueOf( place.getLat() ), Double.valueOf( place.getLang() ) ), unit );
        tvDistance.setText( distance );
        locationDetails.setText( place.getAddress() );

        tvPlaceName.setText( place.getLocationName() );
        ratingPlace.setRating( (float) place.getRating() );
        tvCountry.setText( "Israel" );
        typePlace.setText( place.getType() );
        sendImagesQuery( place.getApiId() );
        ArrayList<Place> ar = new ArrayList<>();
        if (ar.contains( place.getApiId() )) {
            saveToFavoriteBtn.setColorFilter( getResources().getColor( android.R.color.holo_red_dark ) );
        }
    }

    public void sendLiveDataQuery(String placeId) {
        String str = Constants.getPlaceLiveDataQuery( placeId, getResources().getString( R.string.google_maps_key ) );
        GoogleApiService.onSendPlaceLiveData( this, str );
    }

    public void sendPlaceIdQuery(String placeId) {
        GoogleApiService.onSendQueryByPlaceId( this, Constants.getPlaceFullDetails( placeId, getResources().getString( R.string.google_maps_key ) ) );
    }

    public void sendImagesQuery(String placeId) {
        createIntentFilter();
        GoogleApiService.onSendImagesByPlaceIdQuery( this, Constants.sendImagesRequest( placeId, getResources().getString( R.string.google_maps_key ) ) );
    }

    public void imagesRecyclerView(ArrayList<String> list) {
        mImageRecyclerView = findViewById( R.id.rowImages );
        layoutManager = new LinearLayoutManager( this, LinearLayoutManager.HORIZONTAL, true );
        mImageRecyclerView.setLayoutManager( layoutManager );
        mImageRecyclerView.setHasFixedSize( true );
        mPhotosPlaceAdapter = new PhotosPlaceAdapter( this, list, this, mImageRecyclerView );
        mImageRecyclerView.setAdapter( mPhotosPlaceAdapter );
    }

    public void createIntentFilter() {
        IntentFilter filter = new IntentFilter( GoogleApiService.BROADCAST_FOR_SERVICE_FINISHED_RESPONSE_API_ID );
        receiver = new ServiceFinishedBroadcastReceiver();
        LocalBroadcastManager.getInstance( this ).registerReceiver( receiver, filter );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.streetView:
                Intent intent = new Intent( this, StreetViewActivity.class );
                if (place != null) {
                    intent.putExtra( "LAT", place.getLat() );
                    intent.putExtra( "LANG", place.getLang() );
                    startActivity( intent );
                }
                break;
            case R.id.saveToFavoriteBtn:
                clickOnFavoriteBtnColor();
                break;
            case R.id.shareBtn:
                composeEmail( getString( R.string.share_intent_subject ), null, place.getAddress() + "\n\n" + Constants.URL_STORE );
                break;
            case R.id.navigateBtn:
                AlertDialog.Builder builder = new AlertDialog.Builder( this );
                builder.setTitle( R.string.feature_does_not_exist )
                        .setMessage( R.string.we_are_sorry_this_feature_will_be_available_in_the_next_version )
                        .setPositiveButton( R.string.ok, null ).show();
                break;
        }
    }

    public void clickOnFavoriteBtnColor() {
        if (!appReference.getHandler().checkIfPlacesExitsInMyFavoriteDB( place.getApiId() )) {
            if (place.getImagePlace().length() > 10) {
                GlideApp.with( this ).asBitmap().load( Constants.sendImageRequest( place.getImagePlace(), getResources().getString( R.string.google_maps_key ) ) ).into( new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        String placeImagePath = fileHelper.saveImage( resource, place.getImagePlace() );
                        place.setImagePlace( placeImagePath );

                    }
                } );
                appReference.getHandler().insertPlace( place, DBConstants.TABLE_NAME_FAVORITE );
                saveToFavoriteBtn.setColorFilter( getResources().getColor( android.R.color.holo_red_dark ) );
                Snackbar.make( coordinatorLayout, R.string.add_place_to_favorite_list_snackbar, Snackbar.LENGTH_SHORT ).show();

            } else {
                appReference.getHandler().insertPlace( place, DBConstants.TABLE_NAME_FAVORITE );
                saveToFavoriteBtn.setColorFilter( getResources().getColor( android.R.color.holo_red_dark ) );
                Snackbar.make( coordinatorLayout, R.string.add_place_to_favorite_list_snackbar, Snackbar.LENGTH_SHORT ).show();
            }


        } else {
            appReference.getHandler().deletePlace( String.valueOf( place.getApiId() ), DBConstants.TABLE_NAME_FAVORITE );
            saveToFavoriteBtn.setColorFilter( null );
            Snackbar.make( coordinatorLayout, R.string.remove_place_from_favorite_list_snackbar, Snackbar.LENGTH_SHORT ).show();

        }
    }

    public void setFavoriteBtnColor() {
        if (place != null) {
            if (appReference.getHandler().checkIfPlacesExitsInMyFavoriteDB( place.getApiId() )) {
                saveToFavoriteBtn.setColorFilter( getResources().getColor( android.R.color.holo_red_dark ) );

            } else {
                saveToFavoriteBtn.setColorFilter( null );

            }
        }
    }

    public void composeEmail(String subject, Uri attachment, String text) {
        Intent intent = new Intent( Intent.ACTION_SEND );
        intent.setType( "text/plain" );
        intent.putExtra( Intent.EXTRA_SUBJECT, subject );
        intent.putExtra( Intent.EXTRA_STREAM, attachment );
        intent.putExtra( Intent.EXTRA_TEXT, text );
        try {
            startActivity( intent );
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled( false );


        showPlace( place );
    }


    public class ServiceFinishedBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String who = intent.getStringExtra( Constants.FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY );
            String str = intent.getStringExtra( "FromService" );
            if (who.equals( Constants.FRAGMENT_PLACES_NAME_IMAGES_QUERY )) {
                imagesArray = new ArrayList<>();
                if (str != null) {
                    try {
                        JSONObject object = new JSONObject( str );
                        JSONObject array = object.getJSONObject( "result" );
                        String imgReferencePath = "";
                        if (!array.isNull( "photos" )) {
                            JSONArray imgArray = array.getJSONArray( "photos" );
                            for (int i = 0; i < imgArray.length(); i++) {
                                JSONObject imgObject = imgArray.getJSONObject( i );
                                imgReferencePath = imgObject.getString( "photo_reference" );
                                imagesArray.add( imgReferencePath );
                            }
                        }
                        imagesRecyclerView( imagesArray );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (who.equals( Constants.FRAGMENT_PLACES_NAME_PLACE_ID_QUERY )) {
                place = DownloadUrl.getJsonPlaceData( str, appReference.getPreferenceHelper().getUserPreferenceLanLat(), appReference.getPreferenceHelper().getPreferenceUnitsApp() );
                if (place != null)
                    setData( place );
                showPlace( place );
            } else if (who.equals( "LIVE_DATA" )) {
                JSONObject object = null;
                try {
                    object = new JSONObject( str );
                    JSONObject placeResult = object.getJSONObject( "result" );
                    if (!placeResult.isNull( "opening_hours" )) {
                        JSONObject arrayOpeningTime = placeResult.getJSONObject( "opening_hours" );
                        if (arrayOpeningTime.getBoolean( "open_now" )) {
                            isOpen.setText( R.string.open );
                            isOpen.setTextColor( getResources().getColor( android.R.color.holo_green_light ) );
                        } else {
                            isOpen.setText( R.string.close );
                            isOpen.setTextColor( getResources().getColor( android.R.color.holo_red_dark ) );

                        }
                    } else {
                        isOpen.setText( "" );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
