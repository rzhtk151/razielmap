package com.raz.razht.razielmap.util;

import com.google.android.gms.maps.model.LatLng;

/**
 * This class contains constant information which is used throughout the application.
 *
 * @author Raziel Shushan
 */
public class Constants {


    /**
     * Preference activity Setting constants
     */
    public static final String DISTANCE_UNITS_KM = "km";
    public static final String DISTANCE_UNITS_MILES = "miles";
    public static final String LANGUAGE_APP_HEB = "iw";
    public static final String LANGUAGE_AP_EN = "en";

    /**
     * Simple dialog bundle keys
     */
    public static final String DIALOG_MESSAGE = "dialog_message";
    public static final String DIALOG_TITLE = "dialog_title";
    public static final String PLACE = "PLACE";

    /**
     * Type constants
     */
    public static final String TYPE_QUERY_COFFEE = "cafe";
    public static final String TYPE_QUERY_RESTAURANT = "restaurant";
    public static final String TYPE_QUERY_BAR = "bar";
    public static final String TYPE_QUERY_GAS_STATION = "gas_station";
    public static final String TYPE_QUERY_HOTEL = "Sheraton";
    public static final String APP_DB = "app_db";


    public final static String FRAGMENT_MAP_NAME = "MAP";
    public final static String FRAGMENT_FAVORITE_NAME = "FAVORITE";
    public final static String FRAGMENT_PLACES_NAME_IMAGES_QUERY = "PLACES_IMAGES_QUERY";
    public final static String FRAGMENT_PLACES_NAME_PLACE_ID_QUERY = "PLACES_PLACE_ID_QUERY";
    public final static String FRAGMENT_LIST_NAME = "LIST";
    public final static String FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY = "WHO";
    public final static String YOUR_LOCATION = "Your location";
    //public final static String API_KEY =  R.string.google_maps_key ;
    public final static String EXTRA_PATH_IMAGE = "PATH";

    public static final String URL_EMAIL = "mailto:rzhtk151@gmail.com";
    public static final String URL_STORE = "market://details?id=com.raz.razht.razielmap";
    public static final String URL_STORE_RAZIEL_MOVIE_APP = "market://details?id=com.raziel.razielmovie";
    public static final String URL_STORE_BACKUP = "https://play.google.com/store/apps/details?id=com.raz.razht.razielmap";
    public static final String URL_SOURCE_CODE = "https://bitbucket.org/rzhtk151/razielmap/src/master/";
    public static final String URL_LINKEDIN = "https://www.linkedin.com/in/raziel-shushan-446162155/";

    public static final int BACK_BUTTON_DOUBLE_TAP_INTERVAL = 1000; // Milliseconds (1000 ms = 1 sec)


    /**
     * Google maps Api Query to get Image place
     *
     * @param imagePath - which image get
     */
    public final static String sendImageRequest(String imagePath, String apiKey) {
        return "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&" +
                "photoreference=" + imagePath + "&key=" + apiKey;
    }

    /**
     * Google maps Api Query to get max 10 Images list place
     *
     * @param placeId - which place to get
     */
    public final static String sendImagesRequest(String placeId, String apiKey) {
        return "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeId +
                "&fields=photo&key=" + apiKey;
    }

    public static String nearByPlacesQuery(LatLng latLng, String type, int radius, String apiKey) {
        StringBuilder sb = new StringBuilder( "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" );
        sb.append( "location=" + latLng.latitude + "," + latLng.longitude );
        sb.append( "&radius=" + radius );
        sb.append( "&types=" + type );
        sb.append( "&sensor=true" );
        sb.append( "&key=" + apiKey );
        String str = String.valueOf( sb );
        return str;
    }

    /**
     * Google maps Api Query to get place fullDetails
     *
     * @param placeId which place to get
     */
    public static String getPlaceFullDetails(String placeId, String apiKey) {
        StringBuilder sb = new StringBuilder( "https://maps.googleapis.com/maps/api/place/details/json?" );
        sb.append( "key=" + apiKey );
        sb.append( "&placeid=" + placeId );
        String str = String.valueOf( sb );
        return str;
    }

    /**
     * Google maps Api Query to return if the place is open now
     */
    public static String getPlaceLiveDataQuery(String placeId, String apiKey) {
        StringBuilder sb = new StringBuilder( "https://maps.googleapis.com/maps/api/place/details/json?" );
        sb.append( "placeid=" + placeId + "&fields=opening_hours" );
        sb.append( "&key=" + apiKey );
        String str = String.valueOf( sb );
        return str;
    }


}
