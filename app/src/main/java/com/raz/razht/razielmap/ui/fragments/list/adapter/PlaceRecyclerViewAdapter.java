package com.raz.razht.razielmap.ui.fragments.list.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.model.LatLng;
import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.GlideApp;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.db.DBHandler;
import com.raz.razht.razielmap.db.FileHelper;
import com.raz.razht.razielmap.db.Place;
import com.raz.razht.razielmap.ui.activity.main.MainActivity;
import com.raz.razht.razielmap.ui.dialog.SimpleDialog;
import com.raz.razht.razielmap.ui.place.PlaceActivity;
import com.raz.razht.razielmap.util.Constants;
import com.raz.razht.razielmap.util.DownloadUrl;

import java.util.ArrayList;

public class PlaceRecyclerViewAdapter extends RecyclerView.Adapter<PlaceRecyclerViewAdapter.placeViewHolder> {
    Activity activity;
    DBHandler handler;
    App appReference;
    SimpleDialog.ListenerForDialog listener;
    MainActivity mainActivity;
    FileHelper fileHelper;
    View view;
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<Place> dataSet;


    public PlaceRecyclerViewAdapter(View view, Context context, RecyclerView recyclerView, ArrayList<Place> dataSet, Activity activity) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.dataSet = dataSet;
        this.handler = new DBHandler( context );
        this.activity = activity;
        appReference = (App) context.getApplicationContext();
        mainActivity = (MainActivity) context;
        fileHelper = new FileHelper( context );
        this.view = view;
    }

    @NonNull
    @Override
    public PlaceRecyclerViewAdapter.placeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from( parent.getContext() ).inflate( R.layout.row_list, parent, false );
        placeViewHolder viewHolder = new placeViewHolder( v );
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull placeViewHolder holder, int position) {
        holder.onBind( position, dataSet.get( position ) );
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void setFilter(ArrayList<Place> places) {
        dataSet = new ArrayList<>();
        dataSet.addAll( places );
        notifyDataSetChanged();
    }

    public class placeViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgPoster;
        private TextView placeName;
        private TextView placeAddress;
        private TextView distance;
        private TextView rate;
        private CardView cardView;
        private RatingBar ratingBar;

        public placeViewHolder(View itemView) {
            super( itemView );
            imgPoster = itemView.findViewById( R.id.image_row );
            placeName = itemView.findViewById( R.id.name_place_row );
            placeAddress = itemView.findViewById( R.id.address_row );
            distance = itemView.findViewById( R.id.distance_row );
            rate = itemView.findViewById( R.id.rate_row );
            cardView = itemView.findViewById( R.id.card_view );
            ratingBar = itemView.findViewById( R.id.rewRatingPlace );
        }

        public void onBind(int position, Place place) {
            if (place.getImagePlace().length() > 10) {
                String s = Constants.sendImageRequest( place.getImagePlace(), context.getResources().getString( R.string.google_maps_key ) );
                try {
                    GlideApp.with( context ).load( Constants.sendImageRequest( place.getImagePlace(), context.getResources().getString( R.string.google_maps_key ) ) ).into( imgPoster );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            placeName.setText( place.getLocationName() );
            placeAddress.setText( place.getAddress() );
            rate.setText( place.getRating() + "" );
            LatLng latLngUser = appReference.getPreferenceHelper().getUserPreferenceLanLat();
            LatLng placeLatLon = new LatLng( Double.parseDouble( place.getLat() ), Double.parseDouble( place.getLang() ) );
            String newDistance = DownloadUrl.CalculationByDistance( placeLatLon, latLngUser, appReference.getPreferenceHelper().getPreferenceUnitsApp() );
            distance.setText( newDistance );
            ratingBar.setRating( (float) place.getRating() );


            cardView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainActivity.isLargeScreen()) {
                        mainActivity.mOnShowPlaceOnMapListener.onPlaceSelectedForMap( place );
                    } else {
                        Intent intent = new Intent( context, PlaceActivity.class );
                        intent.putExtra( "PLACE_ID", place.getApiId() );
                        intent.putExtra( "PLACE", place );
                        //intent.putExtra(PlaceActivity.EXTRA_CONTENT, content);
                        Pair<View, String> pair1 = Pair.create( activity.findViewById( R.id.image_row ), "image_place" );
                        Pair<View, String> pair2 = Pair.create( activity.findViewById( R.id.distance_row ), "distance_place" );
                        Pair<View, String> pair3 = Pair.create( activity.findViewById( R.id.name_place_row ), "name_place" );

                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation( activity, pair1, pair2, pair3 );
                        ((Activity) context).startActivityForResult( intent, Activity.RESULT_OK, options.toBundle() );
                    }
                }
            } );

            cardView.setOnLongClickListener( v -> {

                AlertDialog.Builder addPlaceToFavoriteDialog = new AlertDialog.Builder( context );
                addPlaceToFavoriteDialog.setMessage( R.string.add_This_Place_to_favorite );
                addPlaceToFavoriteDialog.setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (appReference.getHandler().checkIfPlacesExitsInMyFavoriteDB( place.getApiId() )) {
                            Snackbar.make( view, R.string.place_exits_in_your_favorite_place, Snackbar.LENGTH_SHORT ).show();

                        } else {
                            Place placeSave = place;

                            if (placeSave.getImagePlace().length() > 10) {
                                GlideApp.with( context ).asBitmap().load( Constants.sendImageRequest( place.getImagePlace(), context.getResources().getString( R.string.google_maps_key ) ) ).into( new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                        String placeImagePath = fileHelper.saveImage( resource, place.getImagePlace() );
                                        placeSave.setImagePlace( placeImagePath );

                                    }
                                } );
                                appReference.getHandler().insertPlace( place, DBConstants.TABLE_NAME_FAVORITE );
                                Snackbar.make( view, R.string.add_place_to_favorite_list_snackbar, Snackbar.LENGTH_SHORT ).show();
                                if (!mainActivity.isLargeScreen()) {
                                    mainActivity.mOnChangeFavoriteListener.addNewPlace();
                                }
                            } else {
                                appReference.getHandler().insertPlace( place, DBConstants.TABLE_NAME_FAVORITE );
                            }
                        }

                    }
                } );

                addPlaceToFavoriteDialog.setNegativeButton( "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                } );
                addPlaceToFavoriteDialog.show();
                return true;
            } );
        }
    }
}
