package com.raz.razht.razielmap.ui.fragments.map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.data.preference.PreferenceHelper;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.db.Place;
import com.raz.razht.razielmap.ui.activity.main.MainActivity;
import com.raz.razht.razielmap.ui.place.PlaceActivity;
import com.raz.razht.razielmap.util.Constants;

import java.util.ArrayList;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MapFragment extends SupportMapFragment implements GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener,
        OnMapReadyCallback, MainActivity.OnMapDataReceivesListener, OnShowPlaceOnMap
        , GoogleMap.OnInfoWindowClickListener {
    OnMapReadyCallback onMapReadyCallback;
    Bundle bundle;
    MainActivity mActivity;
    GoogleMap mMap;
    String type;
    App appReference;
    ArrayList<Place> arrayListPlaces;

    public static MapFragment newInstance() {
        MapFragment f = new MapFragment();
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
        bundle = getArguments();
        onMapReadyCallback = (OnMapReadyCallback) context;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate( bundle );
        mActivity = (MainActivity) getActivity();
        mActivity.setMapDataListener( this );
        appReference = (App) getActivity().getApplication();
        arrayListPlaces = new ArrayList<>();
        mActivity.setOnShowPlaceOnMap( this );

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        getMapAsync( this );
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        onMapReadyCallback.onMapReady( googleMap );
        mMap = googleMap;
        onSetDataFromDB( appReference.getPreferenceHelper().getTypeModePref() );
        if (ActivityCompat.checkSelfPermission( getContext(), ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled( true );
        mMap.setOnMyLocationButtonClickListener( this );
        mMap.setOnMyLocationClickListener( this );
        mMap.setPadding( 0, 180, 25, 0 );
        mMap.setOnInfoWindowClickListener( this );
    }

    @Override
    public void onDataReceived(Intent intent) {
        if (mMap != null)
            type = appReference.getPreferenceHelper().getTypeModePref();
        String unit = appReference.getPreferenceHelper().getPreferenceUnitsApp();
        intent.putExtra( PreferenceHelper.PREFERENCE_UNITS_APP_NAME, unit );
        onSetDataFromDB( appReference.getPreferenceHelper().getTypeModePref() );
    }

    @Override
    public void onSetDataFromDB(String type) {
        switch (type) {
            case Constants.TYPE_QUERY_BAR:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_BAR );
                break;
            case Constants.TYPE_QUERY_RESTAURANT:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_RESTAURANT );
                break;
            case Constants.TYPE_QUERY_GAS_STATION:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_GAS_STATION );
                break;
            case Constants.TYPE_QUERY_HOTEL:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_HOTEL );
                break;
            case Constants.TYPE_QUERY_COFFEE:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_CAFE );
                break;
        }
        setDataOnMap( arrayListPlaces, type );

    }

    public void setDataOnMap(ArrayList<Place> list, String type) {
        mMap.clear();
        for (int i = 0; i < list.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();
            String lat = list.get( i ).getLat();
            String lng = list.get( i ).getLang();
            String placeName = list.get( i ).getLocationName();
            LatLng latLng = new LatLng( Double.parseDouble( lat ), Double.parseDouble( lng ) );
            markerOptions.position( latLng );
            markerOptions.title( placeName );
            switch (type) {
                case Constants.TYPE_QUERY_BAR:
                    markerOptions.icon( bitmapDescriptorFromVector( getContext(), R.drawable.ic_local_bar_black_24dp ) );
                    break;
                case Constants.TYPE_QUERY_COFFEE:
                    markerOptions.icon( bitmapDescriptorFromVector( getContext(), R.drawable.ic_local_cafe_black_24dp ) );
                    break;
                case Constants.TYPE_QUERY_RESTAURANT:
                    markerOptions.icon( bitmapDescriptorFromVector( getContext(), R.drawable.ic_restaurant_menu_black_24dp ) );
                    break;
                case Constants.TYPE_QUERY_HOTEL:
                    markerOptions.icon( bitmapDescriptorFromVector( getContext(), R.drawable.ic_hotel_black_24dp ) );
                    break;
                case Constants.TYPE_QUERY_GAS_STATION:
                    markerOptions.icon( bitmapDescriptorFromVector( getContext(), R.drawable.ic_local_gas_station_black_24dp ) );
                    break;
                default:
                    markerOptions.icon( bitmapDescriptorFromVector( getContext(), R.drawable.ic_restaurant_menu_black_24dp ) );
                    break;
            }
            mMap.addMarker( markerOptions );
            mMap.animateCamera( CameraUpdateFactory.zoomTo( 15 ) );
            if (i < 1) {
                mMap.moveCamera( CameraUpdateFactory.newLatLng( latLng ) );
                Circle circle = mMap.addCircle( new CircleOptions().center( latLng ).radius( Integer.parseInt( appReference.getPreferenceHelper().getPreferenceRadiusNumber() ) * 2 )
                        .strokeColor( Color.BLUE ) );
            }
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable vectorDrawable = ContextCompat.getDrawable( context, vectorDrawableResourceId );
        vectorDrawable.setBounds( 0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight() );
        Bitmap bitmap = Bitmap.createBitmap( vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888 );
        Canvas canvas = new Canvas( bitmap );
        vectorDrawable.draw( canvas );
        return BitmapDescriptorFactory.fromBitmap( bitmap );
    }


    public void focusOnLatLan(Place place) {
        if (place != null) {
            LatLng latLng = new LatLng( Double.parseDouble( place.getLat() ), Double.parseDouble( place.getLang() ) );
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position( latLng );
            markerOptions.title( place.getLocationName() );
            mMap.addMarker( markerOptions );
            mMap.animateCamera( CameraUpdateFactory.zoomTo( 16.0f ) );
            float zoomLevel = 16.0f;
            mMap.moveCamera( CameraUpdateFactory.newLatLngZoom( latLng, zoomLevel ) );


        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return true;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        mMap.moveCamera( CameraUpdateFactory.zoomTo( 16 ) );
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String title = marker.getTitle();
        Intent intent = new Intent( getContext(), PlaceActivity.class );
        String placeId = appReference.getHandler().getPlaceIdByName( title, appReference.getPreferenceHelper().getTableName() );
        if (placeId == null)
            return;
        intent.putExtra( "PLACE_ID", placeId );
        startActivity( intent );
    }

    @Override
    public void onPlaceSelectedForMap(Place place) {
        focusOnLatLan( place );
    }
}
