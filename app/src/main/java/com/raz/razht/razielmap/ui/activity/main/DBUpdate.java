package com.raz.razht.razielmap.ui.activity.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.Service.GoogleApiService;
import com.raz.razht.razielmap.data.preference.PreferenceHelper;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.util.Constants;
import com.raz.razht.razielmap.util.DownloadUrl;


public class DBUpdate extends AppCompatActivity {
    App appReference;
    private ServiceFinishedBroadcastReceiver receiver;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        appReference = (App) getApplication();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance( this ).unregisterReceiver( receiver );
    }

    public void createIntentFilter() {
        IntentFilter filter = new IntentFilter( GoogleApiService.BROADCAST_FOR_SERVICE_FINISHED_RESPONSE_NEARBY_PLACES );
        receiver = new ServiceFinishedBroadcastReceiver();
        LocalBroadcastManager.getInstance( this ).registerReceiver( receiver, filter );
    }

    public void updateDBNearByPlaces() {
        GoogleApiService.onNearByPlacesQueryCreateAppDB( this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper().getUserPreferenceLanLat(), Constants.TYPE_QUERY_RESTAURANT, 5000, getResources().getString( R.string.google_maps_key ) ), Constants.APP_DB, Constants.TYPE_QUERY_RESTAURANT );
        GoogleApiService.onNearByPlacesQueryCreateAppDB( this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper().getUserPreferenceLanLat(), Constants.TYPE_QUERY_BAR, 5000, getResources().getString( R.string.google_maps_key ) ), Constants.APP_DB, Constants.TYPE_QUERY_BAR );
        GoogleApiService.onNearByPlacesQueryCreateAppDB( this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper().getUserPreferenceLanLat(), Constants.TYPE_QUERY_HOTEL, 5000, getResources().getString( R.string.google_maps_key ) ), Constants.APP_DB, Constants.TYPE_QUERY_HOTEL );
        GoogleApiService.onNearByPlacesQueryCreateAppDB( this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper().getUserPreferenceLanLat(), Constants.TYPE_QUERY_GAS_STATION, 5000, getResources().getString( R.string.google_maps_key ) ), Constants.APP_DB, Constants.TYPE_QUERY_GAS_STATION );
        GoogleApiService.onNearByPlacesQueryCreateAppDB( this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper().getUserPreferenceLanLat(), Constants.TYPE_QUERY_COFFEE, 5000, getResources().getString( R.string.google_maps_key ) ), Constants.APP_DB, Constants.TYPE_QUERY_COFFEE );
        if (appReference.getPreferenceHelper().getPreferenceNewInApp()) {
            appReference.getPreferenceHelper().setPreferenceNewInApp();
        }
    }


    public class ServiceFinishedBroadcastReceiver extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {
            intent.putExtra( "LOCATION", appReference.getPreferenceHelper().getUserPreferenceLanLat() );
            intent.putExtra( "TYPE", appReference.getPreferenceHelper().getTypeModePref() );
            String who = intent.getStringExtra( Constants.FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY );
            if (who.equals( Constants.FRAGMENT_LIST_NAME )) {
                if (intent.getBooleanExtra( "TOKEN", false )) {
                    // mListDataReceived.onNextPageTokenReceived( intent );
                } else {
                    // mListDataReceived.onDataReceivedFromService( intent );
                }
            } else if (who.equals( Constants.FRAGMENT_MAP_NAME )) {
                //mapDataReceivesListener.onDataReceived( intent );
            } else if (who.equals( Constants.APP_DB )) {
                intent.putExtra( PreferenceHelper.PREFERENCE_UNITS_APP_NAME, appReference.getPreferenceHelper().getPreferenceUnitsApp() );
                switch (intent.getStringExtra( "TYPE" )) {
                    case Constants.TYPE_QUERY_COFFEE:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_CAFE );
                        break;
                    case Constants.TYPE_QUERY_HOTEL:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_HOTEL );
                        break;
                    case Constants.TYPE_QUERY_GAS_STATION:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_GAS_STATION );
                        break;
                    case Constants.TYPE_QUERY_RESTAURANT:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_RESTAURANT );
                        break;
                    case Constants.TYPE_QUERY_BAR:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_BAR );
                        break;
                }
            }
        }
    }

}
