package com.raz.razht.razielmap.data.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDelegate;

import com.google.android.gms.maps.model.LatLng;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.util.Constants;

public class PreferenceHelper {
    /**
     * This is a helper class for dealing with the SharedPreferences throughout the application
     *
     * @author Raziel Shushan
     */

    //preference object
    private final SharedPreferences defaultPreferences;
    private final SharedPreferences typePreference;
    private static PreferenceHelper instance;

    // constants
    public static final String PREFERENCE_NEW_IN_APP = "om.example.razht.razielmap.data.preference.new_in_app";
    public static final String PREFERENCE_SAVE_LANLAT_NAME = "om.example.razht.razielmap.data.preference.save_lanlatname";
    public static final String PREFERENCE_SAVE_LAST_NEARBY_NAME = "om.example.razht.razielmap.data.preference.save_lanlat";
    public static final String PREFERENCE_DELETE_FAVORITES_NAME = "delete_favorites_preference";
    public static final String PREFERENCE_NIGHT_MODE_NAME = "light_mode_preference";
    public static final String PREFERENCE_LANGUAGE_APP_NAME = "language_mode_preference";
    public static final String PREFERENCE_UNITS_APP_NAME = "units_mode_preference";
    public static final String PREFERENCE_RADIUS_APP_NAME = "radius_mode_preference";
    public static final String PREFERENCE_CHANGE_RADIUS_APP_NAME = "change_radius_mode_preference";



    private static final String TYPE_MODE_PREFS_NAME =
            "com.example.razht.razielmap.data.preference.PreferencesHelper.type_preference_name";

    private static final String TYPE_NEAR_MODE_PREF =
            "com.example.razht.razielmap.data.preference.PreferencesHelper.type_near_mode";

    private static final String TYPE_RESTAURANT = "restaurant";


    //constructor
    public PreferenceHelper(@NonNull Context context) {
        defaultPreferences = PreferenceManager.getDefaultSharedPreferences( context );
        typePreference = context.getSharedPreferences( TYPE_MODE_PREFS_NAME, Context.MODE_PRIVATE );
    }

    public static PreferenceHelper getInstance() {
        return instance;
    }

    public static void initialize(Context context) {
        PreferenceHelper.instance = new PreferenceHelper( context );
    }

    /**
     * @return The value of user location if it's found , else return null
     */
    public LatLng getUserPreferenceLanLat() {
        String str = typePreference.getString( PREFERENCE_SAVE_LANLAT_NAME, null );
        if (str != null) {
            String[] perf = str.split( "/" );
            if (perf != null) {
                return new LatLng( Double.parseDouble( perf[0] ), Double.parseDouble( perf[1] ) );
            }
        }

        return null;
    }

    /**
     * insert the user location
     *
     * @param lan a double value
     * @param lat a double value
     */
    public void setUserPreferenceLanLat(@NonNull double lan, @NonNull double lat) {
        typePreference.edit().putString( PREFERENCE_SAVE_LANLAT_NAME, lan + "/" + lat + "" ).apply();
    }

    /**
     * @return The value of near by places query location(The value's that the DB and the query are built on ) if it's found , else return null
     */
    public LatLng getPreferenceLanLatNearByPlaces() {
        String str = typePreference.getString( PREFERENCE_SAVE_LAST_NEARBY_NAME, null );
        String[] perf = str.split( "/" );
        if (perf != null) {
            return new LatLng( Double.parseDouble( perf[0] ), Double.parseDouble( perf[1] ) );
        }
        return null;
    }

    /**
     * insert the near by places query location (The value's that the DB and the query are built on )
     *
     * @param lan a double value
     * @param lat a double value
     */
    public void setPreferenceLanLatNearByPlaces(double lan, double lat) {
        typePreference.edit().putString( PREFERENCE_SAVE_LAST_NEARBY_NAME, lan + "/" + lat + "" ).apply();
    }

    /**
     * @return If it's the first time that the user is running this app.
     */
    public boolean getPreferenceNewInApp() {
        return typePreference.getBoolean( PREFERENCE_NEW_IN_APP, true );
    }


    /**
     * @input the user running the app once (its call one time only)
     */
    public void setPreferenceNewInApp() {
        typePreference.edit().putBoolean( PREFERENCE_NEW_IN_APP, false ).apply();
    }

    /**
     * @return the table name from Db that we use now.
     * there is 5 option - restaurant, coffee, bar, hotel, gas station
     */
    public String getTableName() {
        switch (getTypeModePref()) {
            case Constants.TYPE_QUERY_BAR:
                return DBConstants.TABLE_NAME_NEARBY_BAR;
            case Constants.TYPE_QUERY_COFFEE:
                return DBConstants.TABLE_NAME_NEARBY_CAFE;
            case Constants.TYPE_QUERY_GAS_STATION:
                return DBConstants.TABLE_NAME_NEARBY_GAS_STATION;
            case Constants.TYPE_QUERY_HOTEL:
                return DBConstants.TABLE_NAME_NEARBY_HOTEL;
            case Constants.TYPE_QUERY_RESTAURANT:
                return DBConstants.TABLE_NAME_NEARBY_RESTAURANT;
        }
        return null;
    }

    /**
     * the app db contain 5 type option but  The user can see only one type on the List and Map screen
     *
     * @return the app type mode name
     * there is 5 option - restaurant, coffee, bar, hotel, gas station
     */
    public String getTypeModePref() {
        return typePreference.getString( TYPE_NEAR_MODE_PREF, TYPE_RESTAURANT );
    }

    /**
     * the app db contain 5 type option but  The user can see only one type on the List and Map screen
     *
     * @param type is String value -  there is 5 option - restaurant, coffee, bar, hotel, gas station
     * @input the app type mode name
     */
    public void setTypeModePref(String type) {
        typePreference.edit().putString( TYPE_NEAR_MODE_PREF, type ).apply();
    }

    /**
     * @return A constant boolean value true if app is in night mode
     * set the app NightMode Happens on the preference setting activity
     */
    public boolean getPreferenceNightMode() {
        return defaultPreferences.getBoolean( PREFERENCE_NIGHT_MODE_NAME, false );
    }


    public String getPreferenceRadiusNumber() {
        return defaultPreferences.getString( PREFERENCE_RADIUS_APP_NAME, "1000" );
    }

    public boolean getChangePreferenceRadius() {
        return typePreference.getBoolean( PREFERENCE_CHANGE_RADIUS_APP_NAME, false );
    }

    public void setChangePreferenceRadius(boolean b) {
        typePreference.edit().putBoolean( PREFERENCE_CHANGE_RADIUS_APP_NAME, b ).apply();
    }

    /**
     * @return A constant value for either night mode enabled or disabled.
     */
    public int getTheme() {
        boolean isNightMode = getPreferenceNightMode();
        if (isNightMode)
            return AppCompatDelegate.MODE_NIGHT_YES;

        return AppCompatDelegate.MODE_NIGHT_NO;
    }

    /**
     * @return A constant value for the app language.
     * set the app language Happens on the preference setting activity
     */
    public String getPreferenceLanguageApp() {
        return defaultPreferences.getString( PREFERENCE_LANGUAGE_APP_NAME, Constants.LANGUAGE_AP_EN );
    }

    /**
     * @return A constant value for the app units for distance measurement method in the application
     * There are 2 option -
     * 1 - kilometer = km(return value)
     * 2 - mile = mile(return value)
     * set the app unit Happens on the preference setting activity
     */
    public String getPreferenceUnitsApp() {
        return defaultPreferences.getString( PREFERENCE_UNITS_APP_NAME, Constants.DISTANCE_UNITS_KM );
    }

}
