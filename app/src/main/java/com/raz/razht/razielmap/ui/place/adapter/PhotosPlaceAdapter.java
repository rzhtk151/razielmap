package com.raz.razht.razielmap.ui.place.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.GlideApp;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.ui.place.FullScreenImageActivity;
import com.raz.razht.razielmap.util.Constants;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class PhotosPlaceAdapter extends RecyclerView.Adapter<PhotosPlaceAdapter.PhotoPlaceViewHolder> {

    App appReference;
    private ArrayList<String> dataSet;
    private Context context;
    private RecyclerView recyclerView;
    private Activity activity;


    public PhotosPlaceAdapter(Activity mActivity, ArrayList<String> dataSet, Context context, RecyclerView recyclerView) {
        this.dataSet = dataSet;
        this.context = context;
        activity = mActivity;
        this.recyclerView = recyclerView;
        appReference = (App) context.getApplicationContext();

    }

    @NonNull
    @Override
    public PhotoPlaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( context ).inflate( R.layout.images_place, parent, false );
        PhotoPlaceViewHolder photoPlaceViewHolder = new PhotoPlaceViewHolder( view );
        ButterKnife.bind( this, view );
        return photoPlaceViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoPlaceViewHolder holder, int position) {
        holder.onBind( position, dataSet.get( position ) );

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public class PhotoPlaceViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageButton;
        private View view;

        public PhotoPlaceViewHolder(View itemView) {
            super( itemView );
            imageButton = itemView.findViewById( R.id.images_place );
            view = itemView;

        }

        public void onBind(int position, String s) {
            String str = Constants.sendImageRequest( s, context.getString( R.string.google_maps_key ) );
            if (str.length() > 10) ;
            GlideApp.with( context ).load( str ).into( imageButton );

            imageButton.setOnClickListener( v ->
                    GlideApp.with( context ).asBitmap().load( str ).into( new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            Intent fullScreenIntent = new Intent( context, FullScreenImageActivity.class );
                            Pair<View, String> pair1 = Pair.create( view.findViewById( R.id.images_place ), "image_place" );
                            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation( activity, pair1 );
                            fullScreenIntent.putExtra( "PATH", appReference.getFile().saveImage( resource, s ) );
                            ((Activity) context).startActivityForResult( fullScreenIntent, Activity.RESULT_OK, options.toBundle() );
                        }
                    } ) );
        }
    }

}
