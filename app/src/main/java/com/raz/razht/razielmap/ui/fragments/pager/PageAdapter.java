package com.raz.razht.razielmap.ui.fragments.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import com.raz.razht.razielmap.ui.fragments.favorite.FavoriteFragment;
import com.raz.razht.razielmap.ui.fragments.list.ListFragment;
import com.raz.razht.razielmap.ui.fragments.map.MapFragment;

public class PageAdapter extends FragmentPagerAdapter {
    private int numOfTabs;
    private FragmentManager fragmentManager;

    public PageAdapter(FragmentManager fm, int numOfTabs) {
        super( fm );
        this.fragmentManager = fm;
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ListFragment fragment = ListFragment.newInstance();
                return fragment;
            case 1:
                MapFragment mapFragment = MapFragment.newInstance();
                return mapFragment;
            case 2:
                FavoriteFragment favoriteFragment = FavoriteFragment.newInstance();
                return favoriteFragment;
            default:
                return null;
        }
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem( container, position, object );

        if (position <= getCount()) {
            FragmentManager manager = ((Fragment) object).getFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove( (Fragment) object );
            trans.commit();
        }
    }


    @Override
    public int getCount() {
        return 3;
    }
}
