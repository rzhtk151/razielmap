package com.raz.razht.razielmap.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.db.Place;
import com.raz.razht.razielmap.ui.activity.main.MainActivity;
import com.raz.razht.razielmap.util.Constants;

public class SimpleDialog extends DialogFragment {
    MainActivity mainActivity;
    public final static String TAG = "simple_alert_dialog";

    public static void show(@NonNull FragmentManager fragmentManager, @NonNull String title, @NonNull String message, @NonNull Place place) {

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment previousDialog = fragmentManager.findFragmentByTag( TAG );
        if (previousDialog != null)
            transaction.remove( previousDialog );

        transaction.addToBackStack( null );

        SimpleDialog simpleDialog = new SimpleDialog();

        Bundle bundle = new Bundle();
        bundle.putParcelable( Constants.PLACE, place );
        bundle.putString( Constants.DIALOG_MESSAGE, message );
        bundle.putString( Constants.DIALOG_TITLE, title );

        simpleDialog.show( transaction, TAG );
    }

    public interface ListenerForDialog {
        void onSimpleDialogOkPressed();
    }

    private ListenerForDialog listenerForDialog;

    public void setDialogListener(ListenerForDialog listenerForDialog) {
        this.listenerForDialog = listenerForDialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        AlertDialog.Builder builder = new AlertDialog.Builder( getContext() )
                .setIcon( R.drawable.app_icon )
                .setTitle( bundle.getString( Constants.DIALOG_TITLE ) )
                .setMessage( bundle.getString( Constants.DIALOG_MESSAGE ) )
                .setPositiveButton( R.string.ok, (dialog, which) ->
                        listenerForDialog.onSimpleDialogOkPressed() )
                .setNegativeButton( R.string.cancel, null );

        return builder.create();
    }

}
