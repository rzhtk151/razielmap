package com.raz.razht.razielmap.ui.dialog;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.db.DBConstants;

public class CustomDialogPreference extends DialogPreference {
    App app;

    public CustomDialogPreference(Context context, AttributeSet attrs) {
        super( context, attrs );
        app = (App) context.getApplicationContext();

        // Set the layout here
        setDialogMessage( R.string.delete_all_your_favorite_storage );
        setPositiveButtonText( android.R.string.ok );
        setNegativeButtonText( android.R.string.cancel );
        setDialogIcon( R.drawable.app_icon );
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        // When the user selects "OK", persist the new value
        if (positiveResult) {
            app.getHandler().deleteAllPlaces( app.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_FAVORITE ), DBConstants.TABLE_NAME_FAVORITE );
        } else {
            // User selected Cancel
        }
    }

}
