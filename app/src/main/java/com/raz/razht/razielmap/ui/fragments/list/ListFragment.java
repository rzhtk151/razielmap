package com.raz.razht.razielmap.ui.fragments.list;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.data.preference.PreferenceHelper;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.db.Place;
import com.raz.razht.razielmap.ui.activity.main.MainActivity;
import com.raz.razht.razielmap.ui.fragments.list.adapter.HideShowScrollListener;
import com.raz.razht.razielmap.ui.fragments.list.adapter.PlaceRecyclerViewAdapter;
import com.raz.razht.razielmap.util.Constants;
import com.raz.razht.razielmap.util.DownloadUrl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This Fragment starts immediately when the mainActivity start.
 * this Fragment present in rew the "nearByPlaces" thet near the user(max -  km)
 * in normal phone mode the fragment present in viewPager and the user can scroll between 3 fragments - favorite, map, list
 * in large screen the fragment created from the xml at the mainActivity - 2 fragment only
 *
 * @author Raziel Shushan
 * @see HideShowScrollListener
 * @see PlaceRecyclerViewAdapter
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class ListFragment extends Fragment implements MainActivity.OnListDataReceivedListener, SwipeRefreshLayout.OnRefreshListener, MainActivity.OnDataFinisRefreshListener {
    //ui object
    @BindView(R.id.noResultMessage)
    TextView noResultMessage;
    @BindView(R.id.refreshNearByPlacesTv)
    TextView refreshNearByPlacesTv;
    @BindView(R.id.progressBar1)
    ProgressBar progressBarRefreshList;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.fragment_list)
    SwipeRefreshLayout mSwipeRefreshLayout;
    View view;


    //reference
    App appReference;
    MainActivity mActivity;

    //recyclerView object
    private PlaceRecyclerViewAdapter placeRecyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Place> arrayListPlaces;

    public static ListFragment newInstance() {
        ListFragment f = new ListFragment();
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_list, container, false );
        ButterKnife.bind( this, view );
        this.view = view;
        mSwipeRefreshLayout.setOnRefreshListener( ListFragment.this );
        mSwipeRefreshLayout.setColorSchemeResources( R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark );
        onSetDataFromDB( appReference.getPreferenceHelper().getTypeModePref() );
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        mActivity = (MainActivity) getActivity();
        mActivity.setListDataListener( this );
        mActivity.setFinishListener( this );
        appReference = (App) getActivity().getApplication();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setVisibilityTv(int mode) {
        noResultMessage.setVisibility( mode );
    }


    /**
     * this function can called to set the recyclerView om listFragment:
     *
     * @param list The ArrayList to show on  screen
     */
    public void recyclerView(ArrayList<Place> list) {
        if (list.size() > 0) {
            layoutManager = new LinearLayoutManager( getContext() );
            mRecyclerView.setOnScrollListener( new HideShowScrollListener() {
                @Override
                public void onHide() {
                    mActivity.setAppBarLayoutVisibility( View.INVISIBLE );
                }

                @Override
                public void onShow() {
                    mActivity.setAppBarLayoutVisibility( View.VISIBLE );

                }
            } );
            mRecyclerView.setLayoutManager( layoutManager );
            mRecyclerView.setHasFixedSize( true );
            placeRecyclerViewAdapter = new PlaceRecyclerViewAdapter( view, getContext(), mRecyclerView, list, getActivity() );
            mRecyclerView.setAdapter( placeRecyclerViewAdapter );
            progressBarRefreshList.setVisibility( View.GONE );
            refreshNearByPlacesTv.setVisibility( View.GONE );
            mRecyclerView.setVisibility( View.VISIBLE );
        }
    }

    /**
     * this function  called to set the data
     * he add to optaion:
     * 1 - the list is 0 - show massage and remove all previous items
     * 2 -this list higher then 0 - shoe the list;
     *
     * @param intent contains the json schema off places list
     */
    @Override
    public void onDataReceivedFromService(Intent intent) {
        String unit = appReference.getPreferenceHelper().getPreferenceUnitsApp();
        intent.putExtra( PreferenceHelper.PREFERENCE_UNITS_APP_NAME, unit );
        arrayListPlaces = DownloadUrl.getJsonData( intent );
        if (arrayListPlaces != null && arrayListPlaces.size() > 0) {
            recyclerView( arrayListPlaces );
            placeRecyclerViewAdapter.notifyDataSetChanged();
            setVisibilityTv( View.INVISIBLE );
        } else {
            setVisibilityTv( View.VISIBLE );
            refreshNearByPlacesTv.setVisibility( View.GONE );
            mRecyclerView.removeAllViewsInLayout();
            placeRecyclerViewAdapter.notifyDataSetChanged();

        }
    }


    /**
     * Override methods to set the data from db
     * called after the user refresh the Db ot change the type
     *
     * @param type to know which table get and show
     */
    @Override
    public void onSetDataFromDB(String type) {
        switch (type) {
            case Constants.TYPE_QUERY_BAR:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_BAR );
                break;
            case Constants.TYPE_QUERY_RESTAURANT:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_RESTAURANT );
                break;
            case Constants.TYPE_QUERY_GAS_STATION:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_GAS_STATION );
                break;
            case Constants.TYPE_QUERY_HOTEL:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_HOTEL );
                break;
            case Constants.TYPE_QUERY_COFFEE:
                arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_NEARBY_CAFE );
                break;
        }
        if (arrayListPlaces != null && arrayListPlaces.size() > 0) {
            recyclerView( arrayListPlaces );
            setVisibilityTv( View.INVISIBLE );
        } else {
            mRecyclerView.setVisibility( View.GONE );
            setVisibilityTv( View.VISIBLE );
        }
    }

    /**
     * Override methods to prepare the fragment layout to show result
     * show progressBar
     */
    @Override
    public void onCleanListNearBy() {
        noResultMessage.setVisibility( View.GONE );
        mRecyclerView.setVisibility( View.GONE );
        progressBarRefreshList.setVisibility( View.VISIBLE );
        refreshNearByPlacesTv.setVisibility( View.VISIBLE );
    }


    @Override
    public void onRefresh() {
        mActivity.onRefresh();
    }


    @Override
    public void finishRefresh() {
        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing( false );
    }


}
