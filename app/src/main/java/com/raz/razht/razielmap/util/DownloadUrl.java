package com.raz.razht.razielmap.util;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.data.preference.PreferenceHelper;
import com.raz.razht.razielmap.db.Place;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;


/**
 * This class is used for handle the network action
 *
 * @author Raziel Shushan
 */
public class DownloadUrl {

    /**
     * this function send http request to get json details
     *
     * @param urlString contain the api query
     */
    public static String sendHttpRequest(String urlString) {
        BufferedReader reader = null;
        HttpsURLConnection httpsURLConnection = null;
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        StringBuilder response = new StringBuilder();

        try {
            URL url = new URL( urlString );
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            if (httpsURLConnection.getResponseCode() != HttpsURLConnection.HTTP_OK) {
                return null;
            }
            inputStream = httpsURLConnection.getInputStream();
            inputStreamReader = new InputStreamReader( inputStream );
            reader = new BufferedReader( inputStreamReader );
            String line;
            while ((line = reader.readLine()) != null) {
                response.append( line );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    inputStreamReader.close();
                    inputStream.close();
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (httpsURLConnection != null) {
                    httpsURLConnection.disconnect();
                }
            }
        }
        return response.toString();

    }

    /**
     * this function handle the receive json details from "sendHttpRequest" function
     *
     * @param intent contain extra
     *               1 - json : "FromService"
     * @return max 20 result of places in specific type
     */
    public static ArrayList<Place> getJsonData(Intent intent) {
        String str = intent.getStringExtra( "FromService" );
        ArrayList<Place> arrayListNearByPlaces = new ArrayList<>();
        LatLng myLatLon = intent.getParcelableExtra( "LOCATION" );
        if (str != null) {
            try {
                JSONObject object = new JSONObject( str );
                JSONArray array = object.getJSONArray( "results" );
                for (int i = 0; i < array.length(); i++) {
                    JSONObject item = array.getJSONObject( i );
                    JSONObject arrayGeo = item.getJSONObject( "geometry" );
                    JSONObject jsonLocation = arrayGeo.getJSONObject( "location" );
                    String lat = jsonLocation.getString( "lat" );
                    String lng = jsonLocation.getString( "lng" );
                    int isOpen = 0;
                    if (!item.isNull( "opening_hours" )) {
                        JSONObject arrayOpeningTime = item.getJSONObject( "opening_hours" );
                        boolean isOpenTmp = arrayOpeningTime.getBoolean( "open_now" );
                        if (isOpenTmp)
                            isOpen = 1;
                    }
                    String type = intent.getStringExtra( "TYPE" );

                    String imgReferencePath = "";
                    double rate = 0;
                    if (!item.isNull( "rating" )) {
                        rate = item.getDouble( "rating" );
                    }
                    if (!item.isNull( "photos" )) {
                        JSONArray imgArray = item.getJSONArray( "photos" );
                        JSONObject imgObject = imgArray.getJSONObject( 0 );
                        imgReferencePath = imgObject.getString( "photo_reference" );
                    }
                    String unit = intent.getStringExtra( PreferenceHelper.PREFERENCE_UNITS_APP_NAME );
                    String distance = CalculationByDistance( myLatLon, new LatLng( Double.valueOf( lat ), Double.valueOf( lng ) ), unit );
                    arrayListNearByPlaces.add( new Place( item.getString( "place_id" ), item.getString( "name" ), item.getString( "vicinity" ),
                            item.getString( "icon" ), imgReferencePath, rate, type, lng, lat, distance, isOpen ) );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return arrayListNearByPlaces;
    }

    /**
     * this function handle the receive json details from "sendHttpRequest" function
     *
     * @param str contain the json
     * @return max 20 place_id result
     * never used
     */
    public static ArrayList<String> getPlaceId(String str) {
        ArrayList<String> lists = new ArrayList<>();
        if (str != null) {
            try {
                JSONObject object = new JSONObject( str );

                JSONArray array = object.getJSONArray( "predictions" );
                for (int i = 0; i < array.length(); i++) {
                    JSONObject item = array.getJSONObject( i );
                    String placeId = item.getString( "place_id" );

                    lists.add( new String( placeId ) );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return lists;
    }

    /**
     * this function calculate the distance between to LatLng
     *
     * @param StartP - start LanLng
     * @param EndP   - end LanLng
     * @param unit   - unit to calculate - km/mile
     *               =     * @return string value whit the distance and the unit String, for example - 2.5km or 5.3miles
     */
    // Points will be converted to radians before calculation
    public static String CalculationByDistance(LatLng StartP, LatLng EndP, String unit) {
        double Radius = 6371.01; //Kilometers
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians( lat2 - lat1 );
        double dLon = Math.toRadians( lon2 - lon1 );
        double a = Math.sin( dLat / 2 ) * Math.sin( dLat / 2 )
                + Math.cos( Math.toRadians( lat1 ) )
                * Math.cos( Math.toRadians( lat2 ) ) * Math.sin( dLon / 2 )
                * Math.sin( dLon / 2 );
        double c = 2 * Math.asin( Math.sqrt( a ) );
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat( "####" );
        int kmInDec = Integer.valueOf( newFormat.format( km ) );
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf( newFormat.format( meter ) );
        Log.i( "Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec );
        String endResult = Radius * c + "";

        if (unit.equals( Constants.DISTANCE_UNITS_KM )) {
            String send = endResult.length() >= 4 ? endResult.substring( 0, 4 ) : endResult;
            return send + "km";

        } else if (unit.equals( Constants.DISTANCE_UNITS_MILES )) {
            String x = convertKmsToMiles( Float.parseFloat( endResult ) ) + "";
            String send = x.length() >= 4 ? x.substring( 0, 3 ) : "";
            return send + "miles";
        }
        return null;
    }


    /**
     * this function convert the km  the distance between to LatLng
     *
     * @param kms - value in float to calculate
     * @return distance in float value
     */
    public static float convertKmsToMiles(float kms) {
        float miles = 0.621371f * kms;
        return miles;
    }

    /**
     * this function handle the receive json details from "sendHttpRequest" function
     *
     * @param str      contain the json
     * @param myLatLon the user location - to calculate the distance between the user to the place
     * @param unit     km or mile
     * @return one place result;
     */
    public static Place getJsonPlaceData(String str, LatLng myLatLon, String unit) {
        Place place = null;
        if (str != null) {
            try {
                JSONObject object = new JSONObject( str );

                JSONObject placeResult = object.getJSONObject( "result" );
                JSONObject arrayGeo = placeResult.getJSONObject( "geometry" );
                JSONObject jsonLocation = arrayGeo.getJSONObject( "location" );
                String lat = jsonLocation.getString( "lat" );
                String lng = jsonLocation.getString( "lng" );
                int isOpen = 0;
                String imgReferencePath = "";

                String placeId = placeResult.getString( "place_id" );
                if (!placeResult.isNull( "photo_reference" )) {
                    imgReferencePath = placeResult.getString( "photo_reference" );
                }
                if (!placeResult.isNull( "photos" )) {
                    JSONArray imgArray = placeResult.getJSONArray( "photos" );
                    JSONObject imgObject = imgArray.getJSONObject( 0 );
                    if (!imgObject.isNull( "photo_reference" ))
                        imgReferencePath = imgObject.getString( "photo_reference" );
                }

                double rate = 0;
                if (!placeResult.isNull( "rating" )) {
                    rate = placeResult.getDouble( "rating" );
                }
                JSONArray JsonArrayType = placeResult.getJSONArray( "types" );
                String type = (String) JsonArrayType.get( 0 );

                String distance = CalculationByDistance( myLatLon, new LatLng( Double.valueOf( lat ), Double.valueOf( lng ) ), unit );
                place = new Place( placeId, placeResult.getString( "formatted_address" ), placeResult.getString( "name" ),
                        placeResult.getString( "icon" ), imgReferencePath, rate, type, lng, lat, distance, isOpen );

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return place;
    }

    /**
     * this function check if the user connect to internet
     *
     * @param context
     * @return true or false
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo( ConnectivityManager.TYPE_WIFI );
            android.net.NetworkInfo mobile = cm.getNetworkInfo( ConnectivityManager.TYPE_MOBILE );

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        } else
            return false;
    }

    /**
     * this creata the AlertDialog for "isConnected" function result
     *
     * @param context
     * @return AlertDialog.Builder
     */
    public static AlertDialog.Builder buildDialog(Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder( context );
        builder.setTitle( R.string.no_internet_connection );
        builder.setMessage( R.string.to_be_able_use_all_the_app_Feature_Recommended_to_have_Mobile_Data_or_wifi_to_network );
        builder.setPositiveButton( R.string.ok, null );

        return builder;
    }

    /**
     * start the "isConnected" function
     */
    public static void checkInternet(Context context) {
        if (!DownloadUrl.isConnected( context )) DownloadUrl.buildDialog( context ).show();

    }

}
