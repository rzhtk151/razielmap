package com.raz.razht.razielmap.ui.activity.main;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialOverlayLayout;
import com.leinardi.android.speeddial.SpeedDialView;
import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.Service.GoogleApiService;
import com.raz.razht.razielmap.data.preference.PreferenceHelper;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.preference.PreferenceActivity;
import com.raz.razht.razielmap.sensors.PowerConnectionReceiver;
import com.raz.razht.razielmap.ui.about.AboutPageActivity;
import com.raz.razht.razielmap.ui.fragments.favorite.FavoriteFragment;
import com.raz.razht.razielmap.ui.fragments.favorite.OnChangeFavoriteListener;
import com.raz.razht.razielmap.ui.fragments.map.OnShowPlaceOnMap;
import com.raz.razht.razielmap.ui.fragments.pager.PageAdapter;
import com.raz.razht.razielmap.ui.place.PlaceActivity;
import com.raz.razht.razielmap.util.Constants;
import com.raz.razht.razielmap.util.DownloadUrl;

import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener, PlaceSelectionListener, android.location.LocationListener, View.OnClickListener {

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public OnChangeFavoriteListener mOnChangeFavoriteListener;
    public OnShowPlaceOnMap mOnShowPlaceOnMapListener;
    Snackbar snackbar;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.activity_main)
    CoordinatorLayout coordinatorLayout;
    @Nullable
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.appBarTabLayout)
    AppBarLayout appBarLayout;
    @Nullable
    @BindView(R.id.tab_list)
    TabItem tabItemList;
    @Nullable
    @BindView(R.id.tab_map)
    TabItem tabItemMap;
    @Nullable
    @BindView(R.id.tab_favorite)
    TabItem tabItemFavorite;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.fabSpeed)
    SpeedDialView fabSpeedDial;
    @BindView(R.id.speedDialOverlay)
    SpeedDialOverlayLayout speedDialOverlay;
    @BindView(R.id.menuBtn)
    ImageButton menuBtn;
    @BindView(R.id.tvType)
    TextView tvType;
    boolean typeChange = false;
    Location mLocation;
    int i = 0;
    FragmentTransaction transaction;
    FavoriteFragment favoriteFragment;
    private boolean doubleTapToExit = false;
    private ServiceFinishedBroadcastReceiver receiver;
    private PageAdapter pageAdapter;
    private GoogleMap map;
    private LocationManager locationManager;
    private App appReference;
    private boolean refresh = false;
    //fragments callBack
    private OnListDataReceivedListener mListDataReceived;
    private OnMapDataReceivesListener mapDataReceivesListener;
    private OnDataFinisRefreshListener mOnDataFinisRefreshListener;
    private long userLocationTime;
    private long lastTimeNearBy = 0;
    private boolean fragmentMode = true;

    BroadcastReceiver br = new PowerConnectionReceiver();



    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appReference = (App) getApplication();
        super.onCreate( savedInstanceState );
        DownloadUrl.checkInternet( this );
        setContentView( R.layout.activity_main );
        createIntentFilter();
        ButterKnife.bind( this );
        NavigationView navigationView = findViewById( R.id.nav_view );
        navigationView.setNavigationItemSelectedListener( this );
        createPlaceAutoComplete();
        locationManager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        menuBtn.setOnClickListener( this );
        setupSpeedDialViewBehaviour();
        tvType.setText( appReference.getPreferenceHelper().getTypeModePref() );

        if (!isLargeScreen()) {
            setViewPager();
            drawerLayout.setDrawerLockMode( DrawerLayout.LOCK_MODE_LOCKED_CLOSED );
        }

        if (ActivityCompat.checkSelfPermission( this, ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }


    }

    private LocationRequest createLocationRequest(LocationRequest locationRequest) {
        locationRequest = new LocationRequest();


        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        locationRequest.setInterval( UPDATE_INTERVAL_IN_MILLISECONDS );

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        locationRequest.setFastestInterval( FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS );

        locationRequest.setPriority( LocationRequest.PRIORITY_HIGH_ACCURACY );
        return locationRequest;
    }

    //tabLayoutFunction
    public void setViewPager() {
        viewPager.setOffscreenPageLimit( 3 );
        viewPager.setClipChildren( false );
        viewPager.setPageMargin( 10 );
        changeTabTextStyle();
        tabLayout.addOnTabSelectedListener( new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem( tab.getPosition() );
                setAppBarLayoutVisibility( View.VISIBLE );
                if (typeChange && tabLayout.getSelectedTabPosition() != 2) {
                    typeChange = false;

                } else if (tabLayout.getSelectedTabPosition() == 2) {
                }

                if (tab.getPosition() == 2) {
                    fabSpeedDial.setVisibility( View.GONE );
                } else {
                    fabSpeedDial.setVisibility( View.VISIBLE );
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        } );
        viewPager.addOnPageChangeListener( new TabLayout.TabLayoutOnPageChangeListener( tabLayout ) );
        viewPager.addOnPageChangeListener( new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
               /* if (typeChange) {
                    typeChange = false;
                    sentQuery( PreferenceHelper.getInstance().getTypeModePref() );
                } else if (tabLayout.getSelectedTabPosition() == 2) {
                    //creata
                }*/
            }


            @Override
            public void onPageScrollStateChanged(int state) {


            }
        } );
        pageAdapter = new PageAdapter( getSupportFragmentManager(), tabLayout.getTabCount() );
        viewPager.setAdapter( pageAdapter );
    }

    public void changeTabTextStyle() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TextView tv = (TextView) LayoutInflater.from( this ).inflate( R.layout.tablayout_tv, null );
            tabLayout.getTabAt( i ).setCustomView( tv );
        }
        tabLayout.setSelectedTabIndicatorColor( Color.parseColor( "#ffffff" ) );
        tabLayout.setSelectedTabIndicatorHeight( (int) (5 * getResources().getDisplayMetrics().density) );
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (locationManager != null)
            locationManager.removeUpdates( this );

        try {
            this.unregisterReceiver( br );
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!isLargeScreen())
            mOnChangeFavoriteListener.addNewPlace();
    }

    public boolean isLargeScreen() {
        if (findViewById( R.id.content_main_large ) == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission( this, ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, 0, 0, this );
        locationManager.requestLocationUpdates( LocationManager.PASSIVE_PROVIDER, 0, 0, this );
        locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, this );

        IntentFilter filter = new IntentFilter( ConnectivityManager.CONNECTIVITY_ACTION );
        filter.addAction( Intent.ACTION_POWER_DISCONNECTED );
        filter.addAction( Intent.ACTION_POWER_CONNECTED );
        this.registerReceiver( br, filter );
    }

    public void createPlaceAutoComplete() {
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry( getString( R.string.il ) )
                .build();
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById( R.id.place_autocomplete_fragment );
        autocompleteFragment.setFilter( typeFilter );
        autocompleteFragment.setHint( getString( R.string.search_hint ) );
        autocompleteFragment.setHasOptionsMenu( true );
        autocompleteFragment.setOnPlaceSelectedListener( this );
    }

    //speedDial
    private void setupSpeedDialViewBehaviour() {
        chooseBarBtn();
        chooseCoffeeBtn();
        chooseRestaurantBtn();
        chooseGasStationBtn();
        chooseHotelBtn();
        setupSpeedDialOnActionSelectedListener();
    }

    private void chooseRestaurantBtn() {
        fabSpeedDial.addActionItem( new SpeedDialActionItem.Builder( R.id.fab_restaurant, R.drawable.ic_restaurant_menu_black_24dp )
                .setLabel( Constants.TYPE_QUERY_RESTAURANT )
                .setLabelClickable( true ).create() );
    }

    private void chooseHotelBtn() {
        fabSpeedDial.addActionItem( new SpeedDialActionItem.Builder( R.id.fab_hotel, R.drawable.ic_hotel_black_24dp )
                .setLabel( Constants.TYPE_QUERY_HOTEL )
                .setLabelClickable( true ).create() );
    }

    private void chooseGasStationBtn() {
        fabSpeedDial.addActionItem( new SpeedDialActionItem.Builder( R.id.fab_gas_station, R.drawable.ic_local_gas_station_black_24dp )
                .setLabel( Constants.TYPE_QUERY_GAS_STATION )
                .setLabelClickable( true ).create() );
    }

    private void chooseCoffeeBtn() {
        fabSpeedDial.addActionItem( new SpeedDialActionItem.Builder( R.id.fab_coffee, R.drawable.ic_local_cafe_black_24dp )
                .setLabel( Constants.TYPE_QUERY_COFFEE )
                .setLabelClickable( true ).create() );
    }

    private void chooseBarBtn() {
        fabSpeedDial.addActionItem( new SpeedDialActionItem.Builder( R.id.fab_bar, R.drawable.ic_local_bar_black_24dp )
                .setLabel( Constants.TYPE_QUERY_BAR )
                .setLabelClickable( true ).create() );
    }

    private void setupSpeedDialOnActionSelectedListener() {
        fabSpeedDial.setOnActionSelectedListener( actionItem -> {
            if (typeChoose( actionItem.getLabel() )) {
                switch (actionItem.getId()) {
                    case R.id.fab_coffee:
                        nearByPlacesSend( Constants.TYPE_QUERY_COFFEE );
                        tvType.setText( R.string.coffee );
                        break;
                    case R.id.fab_restaurant:
                        nearByPlacesSend( Constants.TYPE_QUERY_RESTAURANT );
                        tvType.setText( R.string.restaurant );
                        break;
                    case R.id.fab_bar:
                        nearByPlacesSend( Constants.TYPE_QUERY_BAR );
                        tvType.setText( R.string.bar );
                        ;
                        break;
                    case R.id.fab_gas_station:
                        nearByPlacesSend( Constants.TYPE_QUERY_GAS_STATION );
                        tvType.setText( R.string.gas_station );
                        break;
                    case R.id.fab_hotel:
                        nearByPlacesSend( Constants.TYPE_QUERY_HOTEL );
                        tvType.setText( R.string.hotel );
                        break;
                    default:
                }
            }
            return false;
        } );
    }

    //menu && menu drwer
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_list:
                TabLayout.Tab list = tabLayout.getTabAt( 0 );
                list.select();
                break;
            case R.id.nav_map:
                TabLayout.Tab map = tabLayout.getTabAt( 1 );
                map.select();
                break;
            case R.id.nav_favorite:
                if (isLargeScreen()) {
                    createFavoriteFragment();
                } else {
                    TabLayout.Tab favorite = tabLayout.getTabAt( 2 );
                    favorite.select();
                }
                break;
            case R.id.nav_setting:
                Intent intent = new Intent( this, PreferenceActivity.class );
                finish();
                startActivity( intent );
                break;
            case R.id.nav_share:
                try {
                    Intent i = new Intent( Intent.ACTION_SEND );
                    i.setType( "text/plain" );
                    i.putExtra( Intent.EXTRA_SUBJECT, R.string.app_name );
                    String sAux = "\nRecommend app for you\n\n";
                    sAux = sAux + Constants.URL_STORE_BACKUP + "\n\n";
                    i.putExtra( Intent.EXTRA_TEXT, sAux );
                    startActivity( Intent.createChooser( i, "choose one" ) );
                } catch (Exception e) {
                    e.toString();
                }
                break;
            case R.id.nav_about:
                Intent startAbout = new Intent( this, AboutPageActivity.class );
                startActivity( startAbout );
                break;


        }
        drawerLayout.closeDrawer( GravityCompat.START );
        return false;
    }

    public void createFavoriteFragment() {
        fabSpeedDial.setVisibility( View.GONE );
        transaction = getSupportFragmentManager().beginTransaction();
        favoriteFragment = FavoriteFragment.newInstance();
        transaction.replace( android.R.id.content, favoriteFragment ).addToBackStack( Constants.FRAGMENT_FAVORITE_NAME ).commit();
    }

    public void setListDataListener(OnListDataReceivedListener listener) {
        this.mListDataReceived = listener;
    }


    /*list fragment*/

    public boolean typeChoose(String type) {
        if (!type.equals( appReference.getPreferenceHelper().getTypeModePref() )) {
            typeChange = true;
            PreferenceHelper.getInstance().setTypeModePref( type );
            return true;
        } else {
            return false;
        }

    }

    /*map*/
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    public void setMapDataListener(OnMapDataReceivesListener listener) {
        this.mapDataReceivesListener = listener;
    }

    public void nearByPlacesSend(String type) {
        if (sendNewQuery()) {
            updateDBNearByPlaces();
        } else {
            mapDataReceivesListener.onSetDataFromDB( type );
            mListDataReceived.onSetDataFromDB( type );
        }
    }

    public void onRefresh() {
        if (sendNewQuery()) {
            refresh = true;
            updateDBNearByPlaces();
        } else {
            mOnDataFinisRefreshListener.finishRefresh();
            Snackbar.make( coordinatorLayout, R.string.we_dont_found_new_places_near_you, Snackbar.LENGTH_SHORT ).show();

        }
    }

    public void createIntentFilter() {
        IntentFilter filter = new IntentFilter( GoogleApiService.BROADCAST_FOR_SERVICE_FINISHED_RESPONSE_NEARBY_PLACES );
        receiver = new ServiceFinishedBroadcastReceiver();
        LocalBroadcastManager.getInstance( this ).registerReceiver( receiver, filter );
    }

    //location
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult( requestCode, permissions, grantResults );
        snackbar = Snackbar.make( coordinatorLayout, R.string.massege_location_permission_denied, Snackbar.LENGTH_LONG );
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //snackbar.dismiss();
                    updateDataLocation();
                } else {
                    snackbar.show();
                    requestPermissions();
                }
                break;
        }

    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions( this, new String[]{ACCESS_FINE_LOCATION}, 1 );
    }

    public void updateDataLocation() {
        if (mLocation != null) {
            //mCurrentLocation = mLocationResult.getLastLocation();
            //String mLastUpdateTime = DateFormat.getTimeInstance().format( new Date() );
            double Latitude = mLocation.getLatitude();
            double longitude = mLocation.getLongitude();
            appReference.getPreferenceHelper().setUserPreferenceLanLat( Latitude, longitude );
            if (appReference.getPreferenceHelper().getPreferenceNewInApp()) {
                appReference.getPreferenceHelper().setPreferenceLanLatNearByPlaces( Latitude, longitude );
                updateDBNearByPlaces();
                return;
            }
            if (sendNewQuery()) {
                LatLng p = appReference.getPreferenceHelper().getPreferenceLanLatNearByPlaces();
                LatLng m = appReference.getPreferenceHelper().getUserPreferenceLanLat();
                appReference.getPreferenceHelper().setPreferenceLanLatNearByPlaces( Latitude, longitude );
                updateDBNearByPlaces();
            }

        }
    }

    public boolean sendNewQuery() {
        boolean b = appReference.getPreferenceHelper().getChangePreferenceRadius();
        if (appReference.getPreferenceHelper().getChangePreferenceRadius())
            return true;
        long re = lastTimeNearBy - userLocationTime;
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis( userLocationTime );
        int min2 = calendar.get( Calendar.SECOND );
        Calendar calendar2 = new GregorianCalendar();
        calendar.setTimeInMillis( lastTimeNearBy );
        int min = calendar.get( Calendar.SECOND );
        if (Math.abs( min2 - min ) > 10) {
            lastTimeNearBy = userLocationTime;
            String unit = appReference.getPreferenceHelper().getPreferenceUnitsApp();
            String distance = DownloadUrl.CalculationByDistance( appReference.getPreferenceHelper().getPreferenceLanLatNearByPlaces(), appReference.getPreferenceHelper().getUserPreferenceLanLat(), unit );
            String str = distance.replaceAll( "[^\\.0123456789]", "" );
            double newDistance = Double.valueOf( str );

            if (unit.equals( Constants.DISTANCE_UNITS_MILES )) {
                if (newDistance > 2) {
                    appReference.getPreferenceHelper().setPreferenceLanLatNearByPlaces( appReference.getPreferenceHelper().getUserPreferenceLanLat().latitude, appReference.getPreferenceHelper().getUserPreferenceLanLat().longitude );
                    return true;
                } else {
                    return false;
                }
            } else {
                if (newDistance > 3) {
                    appReference.getPreferenceHelper().setPreferenceLanLatNearByPlaces( appReference.getPreferenceHelper().getUserPreferenceLanLat().latitude, appReference.getPreferenceHelper().getUserPreferenceLanLat().longitude );
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }

    }

    //setThePlacesDb
    public void updateDBNearByPlaces() {
        mListDataReceived.onCleanListNearBy();
        appReference.getPreferenceHelper().setChangePreferenceRadius( false );
        GoogleApiService.onNearByPlacesQueryCreateAppDB( MainActivity.this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper()
                        .getUserPreferenceLanLat(), Constants.TYPE_QUERY_RESTAURANT, Integer.parseInt( appReference.getPreferenceHelper().getPreferenceRadiusNumber() ), getResources().getString( R.string.google_maps_key ) ),
                Constants.APP_DB, Constants.TYPE_QUERY_RESTAURANT );
        GoogleApiService.onNearByPlacesQueryCreateAppDB( MainActivity.this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper().
                        getUserPreferenceLanLat(), Constants.TYPE_QUERY_BAR, Integer.parseInt( appReference.getPreferenceHelper().getPreferenceRadiusNumber() ), getResources().getString( R.string.google_maps_key ) ),
                Constants.APP_DB, Constants.TYPE_QUERY_BAR );
        GoogleApiService.onNearByPlacesQueryCreateAppDB( MainActivity.this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper().
                        getUserPreferenceLanLat(), Constants.TYPE_QUERY_HOTEL, Integer.parseInt( appReference.getPreferenceHelper().getPreferenceRadiusNumber() ), getResources().getString( R.string.google_maps_key ) ),
                Constants.APP_DB, Constants.TYPE_QUERY_HOTEL );
        GoogleApiService.onNearByPlacesQueryCreateAppDB( MainActivity.this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper().
                        getUserPreferenceLanLat(), Constants.TYPE_QUERY_GAS_STATION, Integer.parseInt( appReference.getPreferenceHelper().getPreferenceRadiusNumber() ), getResources().getString( R.string.google_maps_key ) ),
                Constants.APP_DB, Constants.TYPE_QUERY_GAS_STATION );
        GoogleApiService.onNearByPlacesQueryCreateAppDB( MainActivity.this, Constants.nearByPlacesQuery( appReference.getPreferenceHelper().
                        getUserPreferenceLanLat(), Constants.TYPE_QUERY_COFFEE, Integer.parseInt( appReference.getPreferenceHelper().getPreferenceRadiusNumber() ), getResources().getString( R.string.google_maps_key ) ),
                Constants.APP_DB, Constants.TYPE_QUERY_COFFEE );
    }

    public void setAppBarLayoutVisibility(int mode) {
        if (!isLargeScreen())
            appBarLayout.setVisibility( mode );
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen( GravityCompat.START )) {
            drawerLayout.closeDrawer( GravityCompat.START );
        } else if (doubleTapToExit) {

            super.onBackPressed();

        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0 && isLargeScreen()) {
            getSupportFragmentManager().popBackStack( Constants.FRAGMENT_FAVORITE_NAME, FragmentManager.POP_BACK_STACK_INCLUSIVE );

        } else {
            final Snackbar snackbar = Snackbar
                    .make( coordinatorLayout, R.string.double_tap_message, Snackbar.LENGTH_INDEFINITE );
            snackbar.show();
            doubleTapToExit = true;
            new Handler().postDelayed( () -> {
                doubleTapToExit = false;
                snackbar.dismiss();
            }, Constants.BACK_BUTTON_DOUBLE_TAP_INTERVAL );
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance( this ).unregisterReceiver( receiver );

    }

    @Override
    public void onPlaceSelected(Place place) {
        Intent intent = new Intent( this, PlaceActivity.class );
        intent.putExtra( "AUTO_COMPLETE", true );
        intent.putExtra( "PLACE_ID", place.getId() );
        startActivity( intent );
    }

    @Override
    public void onError(Status status) {
        int i = 6;

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            return;
        }
        mLocation = location;
        userLocationTime = location.getTime();
        updateDataLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        int i = 6;

    }

    @Override
    public void onProviderEnabled(String provider) {
        int i = 6;

    }

    @Override
    public void onProviderDisabled(String provider) {
        int i = 6;

    }

    public void setFinishListener(OnDataFinisRefreshListener listener) {
        this.mOnDataFinisRefreshListener = listener;
    }

    public void setOnChangeFavoriteListener(OnChangeFavoriteListener mOnChangeFavoriteListener) {
        this.mOnChangeFavoriteListener = mOnChangeFavoriteListener;
    }

    public void setOnShowPlaceOnMap(OnShowPlaceOnMap mOnShowPlaceOnMap) {
        this.mOnShowPlaceOnMapListener = mOnShowPlaceOnMap;
    }

    public void upDateDataOnFragments() {
        mapDataReceivesListener.onSetDataFromDB( appReference.getPreferenceHelper().getTypeModePref() );
        mListDataReceived.onSetDataFromDB( appReference.getPreferenceHelper().getTypeModePref() );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menuBtn:
                drawerLayout.openDrawer( Gravity.START );
                break;
        }
    }

    public interface OnDataFinisRefreshListener {
        void finishRefresh();
    }

    //refresh favorite fragment listener

    //fragment list interface
    public interface OnListDataReceivedListener {
        void onDataReceivedFromService(Intent intent);

        void onSetDataFromDB(String type);

        void onCleanListNearBy();
    }

    //fragment map interface
    public interface OnMapDataReceivesListener {
        void onDataReceived(Intent intent);

        void onSetDataFromDB(String type);

    }

    public class ServiceFinishedBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            intent.putExtra( "LOCATION", appReference.getPreferenceHelper().getUserPreferenceLanLat() );
            String who = intent.getStringExtra( Constants.FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY );
            if (who.equals( Constants.FRAGMENT_LIST_NAME )) {
                mListDataReceived.onDataReceivedFromService( intent );
            } else if (who.equals( Constants.APP_DB )) {
                intent.putExtra( PreferenceHelper.PREFERENCE_UNITS_APP_NAME, appReference.getPreferenceHelper().getPreferenceUnitsApp() );
                switch (intent.getStringExtra( "TYPE" )) {
                    case Constants.TYPE_QUERY_COFFEE:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_CAFE );
                        if (Constants.TYPE_QUERY_COFFEE.equals( appReference.getPreferenceHelper().getTypeModePref() ))
                            upDateDataOnFragments();
                        break;
                    case Constants.TYPE_QUERY_HOTEL:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_HOTEL );
                        if (Constants.TYPE_QUERY_HOTEL.equals( appReference.getPreferenceHelper().getTypeModePref() ))
                            upDateDataOnFragments();
                        break;
                    case Constants.TYPE_QUERY_GAS_STATION:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_GAS_STATION );
                        if (Constants.TYPE_QUERY_GAS_STATION.equals( appReference.getPreferenceHelper().getTypeModePref() ))
                            upDateDataOnFragments();
                        break;
                    case Constants.TYPE_QUERY_RESTAURANT:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_RESTAURANT );
                        if (Constants.TYPE_QUERY_RESTAURANT.equals( appReference.getPreferenceHelper().getTypeModePref() ))
                            upDateDataOnFragments();
                        break;
                    case Constants.TYPE_QUERY_BAR:
                        appReference.getHandler().insertArrayPlace( DownloadUrl.getJsonData( intent ), DBConstants.TABLE_NAME_NEARBY_BAR );
                        if (Constants.TYPE_QUERY_BAR.equals( appReference.getPreferenceHelper().getTypeModePref() ))
                            upDateDataOnFragments();
                        break;
                }
                ++i;
                if (appReference.getPreferenceHelper().getPreferenceNewInApp() && i == 5) {
                    appReference.getPreferenceHelper().setPreferenceNewInApp();
                    upDateDataOnFragments();
                    i = 0;
                } else if (refresh && i == 5) {
                    refresh = false;
                    upDateDataOnFragments();
                    mOnDataFinisRefreshListener.finishRefresh();
                    Snackbar.make( coordinatorLayout, R.string.we_update_your_data, Snackbar.LENGTH_SHORT ).show();

                }
            }
        }
    }


}
