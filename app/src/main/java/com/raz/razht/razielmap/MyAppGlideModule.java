package com.raz.razht.razielmap;

import android.content.Context;
import android.support.annotation.NonNull;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

/**
 * This class is used by the Glide Image Loading library in order for Glide to work on an
 * application level.
 *
 * @see com.bumptech.glide.module.AppGlideModule
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {

    /**
     * @see <a href="https://bumptech.github.io/glide/doc/hardwarebitmaps.html">
     * Glide Hardware Bitmaps Documentation
     * </a>
     */
    @Override
    public void applyOptions(@NonNull Context context, @NonNull GlideBuilder builder) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.format( DecodeFormat.PREFER_ARGB_8888 );
        requestOptions.disallowHardwareConfig();
        builder.setDefaultRequestOptions( requestOptions );
    }
}
