package com.raz.razht.razielmap.ui.fragments.favorite;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.db.Place;
import com.raz.razht.razielmap.ui.activity.main.MainActivity;
import com.raz.razht.razielmap.ui.fragments.list.adapter.HideShowScrollListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteFragment extends Fragment implements OnChangeFavoriteListener {

    MainActivity mActivity;
    App appReference;
    @BindView(R.id.noResultMessage)
    TextView noResultMessage;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    /*@BindView( R.id.activity_main )
    CoordinatorLayout coordinatorLayout;*/
    private ArrayList<Place> arrayListPlaces;
    private FavoriteRecyclerViewAdapter placeRecyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;

    public static FavoriteFragment newInstance() {
        FavoriteFragment f = new FavoriteFragment();
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        appReference = (App) getContext().getApplicationContext();
        arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_FAVORITE );
        mActivity = (MainActivity) getActivity();
        mActivity.setOnChangeFavoriteListener( this );

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_favorite, container, false );
        ButterKnife.bind( this, view );
        if (arrayListPlaces != null && arrayListPlaces.size() > 0) {
            recyclerView( arrayListPlaces );
            setVisibilityTv( View.INVISIBLE );
        } else {
            setVisibilityTv( View.VISIBLE );
        }
        return view;
    }

    public void setLayoutManagerApp() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            layoutManager = new GridLayoutManager( getContext(), 2 );

        } else {
            layoutManager = new LinearLayoutManager( getContext() );

        }
    }


    public void recyclerView(ArrayList<Place> list) {
        if (list.size() > 0) {
            if (mActivity.isLargeScreen()) {
                setLayoutManagerApp();
            } else {
                layoutManager = new LinearLayoutManager( getContext() );

            }
            setVisibilityTv( View.INVISIBLE );
            //layoutManager = new LinearLayoutManager( getContext() );
            mRecyclerView.setOnScrollListener( new HideShowScrollListener() {
                @Override
                public void onHide() {
                    mActivity.setAppBarLayoutVisibility( View.INVISIBLE );
                }

                @Override
                public void onShow() {
                    mActivity.setAppBarLayoutVisibility( View.VISIBLE );

                }
            } );
            mRecyclerView.setLayoutManager( layoutManager );
            mRecyclerView.setHasFixedSize( true );
            placeRecyclerViewAdapter = new FavoriteRecyclerViewAdapter( getContext(), mRecyclerView, list, getActivity() );
            mRecyclerView.setAdapter( placeRecyclerViewAdapter );
        } else {
            setVisibilityTv( View.VISIBLE );
        }

    }

    public void setVisibilityTv(int mode) {
        noResultMessage.setVisibility( mode );
    }

    @Override
    public void addNewPlace() {
        arrayListPlaces = appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_FAVORITE );
        recyclerView( arrayListPlaces );


    }

    @Override
    public void deletePlace(int position) {
        arrayListPlaces.remove( position );
        placeRecyclerViewAdapter.notifyItemRemoved( position );
        placeRecyclerViewAdapter.notifyItemRangeChanged( position, arrayListPlaces.size() );
        Snackbar.make( getView(), getResources().getString( R.string.remove_place_from_favorite_list_snackbar ), Snackbar.LENGTH_SHORT ).show();

        if (arrayListPlaces.size() == 0) {
            setVisibilityTv( View.VISIBLE );
        }
    }
}
