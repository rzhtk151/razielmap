package com.raz.razht.razielmap.ui.fragments.favorite;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.db.DBHandler;
import com.raz.razht.razielmap.db.FileHelper;
import com.raz.razht.razielmap.db.Place;
import com.raz.razht.razielmap.ui.activity.main.MainActivity;
import com.raz.razht.razielmap.ui.place.PlaceActivity;
import com.raz.razht.razielmap.util.Constants;
import com.raz.razht.razielmap.util.DownloadUrl;

import java.util.ArrayList;

public class FavoriteRecyclerViewAdapter extends RecyclerView.Adapter<FavoriteRecyclerViewAdapter.placeViewHolder> {
    Activity activity;
    DBHandler handler;
    MainActivity mainActivity;
    App appReference;
    FileHelper fileHelper;
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<Place> dataSet;


    public FavoriteRecyclerViewAdapter(Context context, RecyclerView recyclerView, ArrayList<Place> dataSet, Activity activity) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.dataSet = dataSet;
        this.handler = new DBHandler( context );
        this.activity = activity;
        mainActivity = (MainActivity) context;
        appReference = (App) activity.getApplication();
        fileHelper = new FileHelper( context );

    }

    @NonNull
    @Override
    public FavoriteRecyclerViewAdapter.placeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from( parent.getContext() ).inflate( R.layout.row_favorite, parent, false );
        placeViewHolder viewHolder = new placeViewHolder( v );
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull placeViewHolder holder, int position) {
        holder.onBind( position, dataSet.get( position ) );
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void setFilter(ArrayList<Place> places) {
        dataSet = new ArrayList<>();
        dataSet.addAll( places );
        notifyDataSetChanged();
    }

    public class placeViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgPoster;
        private TextView placeName;
        private TextView placeAddress;
        private TextView distance;
        private TextView rate;
        private CardView cardView;
        private RatingBar ratingBar;


        public placeViewHolder(View itemView) {
            super( itemView );
            imgPoster = itemView.findViewById( R.id.image_row );
            placeName = itemView.findViewById( R.id.name_place_row );
            placeAddress = itemView.findViewById( R.id.address_row );
            distance = itemView.findViewById( R.id.distance_row );
            rate = itemView.findViewById( R.id.rate_row );
            ratingBar = itemView.findViewById( R.id.rewRatingPlace );
            cardView = itemView.findViewById( R.id.card_view );


        }

        public void onBind(int position, Place place) {
            String s = Constants.sendImageRequest( place.getImagePlace(), context.getString( R.string.google_maps_key ) );
            fileHelper.loadImageFromStorage( place.getImagePlace(), imgPoster );
            placeName.setText( place.getLocationName() );
            placeAddress.setText( place.getAddress() );
            rate.setText( place.getRating() + "" );
            LatLng latLngUser = appReference.getPreferenceHelper().getUserPreferenceLanLat();
            LatLng placeLatLon = new LatLng( Double.parseDouble( place.getLat() ), Double.parseDouble( place.getLang() ) );
            String newDistance = DownloadUrl.CalculationByDistance( placeLatLon, latLngUser, appReference.getPreferenceHelper().getPreferenceUnitsApp() );
            distance.setText( newDistance );
            ratingBar.setRating( (float) place.getRating() );

            cardView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent( context, PlaceActivity.class );
                    intent.putExtra( "PLACE", place );
                    Pair<View, String> pair1 = Pair.create( activity.findViewById( R.id.image_row ), "image_place" );
                    Pair<View, String> pair2 = Pair.create( activity.findViewById( R.id.distance_row ), "distance_place" );
                    Pair<View, String> pair3 = Pair.create( activity.findViewById( R.id.name_place_row ), "name_place" );

                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation( activity, pair1, pair2, pair3 );
                    ((Activity) context).startActivityForResult( intent, Activity.RESULT_OK, options.toBundle() );
                }
            } );

            cardView.setOnLongClickListener( v -> {

                AlertDialog.Builder deletePlaceFromListDialog = new AlertDialog.Builder( context );
                deletePlaceFromListDialog.setMessage( R.string.you_want_to_delete_place_from_your_list );
                deletePlaceFromListDialog.setPositiveButton( R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.deletePlace( place.getApiId() + "", DBConstants.TABLE_NAME_FAVORITE );
                        mainActivity.mOnChangeFavoriteListener.deletePlace( getAdapterPosition() );
                    }
                } );

                deletePlaceFromListDialog.setNegativeButton( R.string.no, null );

                deletePlaceFromListDialog.show();
                return true;
            } );
        }
    }
}
