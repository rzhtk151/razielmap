package com.raz.razht.razielmap.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.raz.razht.razielmap.db.DBConstants;

import java.util.List;

public interface PlaceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PlaceRoom place);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertListPlace(List<PlaceRoom> place);

    @Delete
    void delete(String apiId);

    @Delete
    void deleteAllFavorite(List<PlaceRoom> list);

    @Query("SELECT * from table_places WHERE " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE + " = :type")
    LiveData<List<PlaceRoom>> getAllPlaces(String type);

    @Query("SELECT * from table_places WHERE " + DBConstants.COLUMN_NAME_API_ID + " = :apiId")
    LiveData<List<PlaceRoom>> selectPlaceByPlaceApiId(String apiId);

    @Query("SELECT * from table_places WHERE " + DBConstants.COLUMN_NAME_LOCATION_NAME + " = :name")
    LiveData<List<PlaceRoom>> getPlaceIdByName(String name);

    @Query("SELECT * from table_places WHERE " + DBConstants.COLUMN_NAME_IS_FAVORITE + " = 1")
    LiveData<List<PlaceRoom>> getAllFavoritePlaces();
}