package com.raz.razht.razielmap.sensors;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.raz.razht.razielmap.R;

/**
 * This class to get the service fro pluggedIn/pluggedOut the power connection
 *
 * @author Raziel Shushan
 */
public class PowerConnectionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == Intent.ACTION_POWER_CONNECTED) {
            //Handle power connected
            Toast.makeText( context, R.string.charger_pluggedIn, Toast.LENGTH_SHORT ).show();
        } else if (intent.getAction() == Intent.ACTION_POWER_DISCONNECTED) {
            //Handle power disconnected
            Toast.makeText( context, R.string.charger_pluggedOut, Toast.LENGTH_SHORT ).show();
        }

    }
}
