package com.raz.razht.razielmap.db;

import android.os.Parcel;
import android.os.Parcelable;

public class Place implements Parcelable {

    private int id;
    private String apiId;
    private String locationName;
    private String address;
    private String icon;
    private String imagePlace;
    private double rating;
    private String type;
    private String lang;
    private String lat;
    private String distance;
    private int isOpen;

    public Place() {
    }


    public Place(String apiId, String locationName, String address, String distance, String type) {
        this.apiId = apiId;
        this.locationName = locationName;
        this.address = address;
        this.distance = distance;
        this.type = type;
    }

    public Place(String apiId, String locationName, String address, String icon, String imagePlace, double rating, String type, String lang, String lat, String distance, int isOpen) {

        this.apiId = apiId;
        this.locationName = locationName;
        this.address = address;
        this.icon = icon;
        this.imagePlace = imagePlace;
        this.rating = rating;
        this.type = type;
        this.lang = lang;
        this.lat = lat;
        this.distance = distance;
        this.isOpen = isOpen;
    }

    public Place(int id, String apiId, String locationName, String address, String icon, String imagePlace, double rating, String type, String lang, String lat, String distance, int isOpen) {
        this.id = id;
        this.apiId = apiId;
        this.locationName = locationName;
        this.address = address;
        this.icon = icon;
        this.imagePlace = imagePlace;
        this.rating = rating;
        this.type = type;
        this.lang = lang;
        this.lat = lat;
        this.distance = distance;
        this.isOpen = isOpen;

    }

    public int getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(int isOpen) {
        this.isOpen = isOpen;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getImagePlace() {
        return imagePlace;
    }

    public void setImagePlace(String imagePlace) {
        this.imagePlace = imagePlace;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt( id );
        out.writeString( apiId );
        out.writeString( locationName );
        out.writeString( address );
        out.writeString( icon );
        out.writeString( imagePlace );
        out.writeDouble( rating );
        out.writeString( type );
        out.writeString( lang );
        out.writeString( lat );
        out.writeString( distance );
        out.writeInt( isOpen );
    }

    public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>() {
        public Place createFromParcel(Parcel in) {
            return new Place( in );
        }

        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Place(Parcel in) {
        id = in.readInt();
        apiId = in.readString();
        locationName = in.readString();
        address = in.readString();
        icon = in.readString();
        imagePlace = in.readString();
        rating = in.readDouble();
        type = in.readString();
        lang = in.readString();
        lat = in.readString();
        distance = in.readString();
        isOpen = in.readInt();
    }
}

