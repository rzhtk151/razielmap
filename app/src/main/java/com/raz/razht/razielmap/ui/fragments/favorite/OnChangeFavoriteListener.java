package com.raz.razht.razielmap.ui.fragments.favorite;

public interface OnChangeFavoriteListener {
    void addNewPlace();

    void deletePlace(int position);
}
