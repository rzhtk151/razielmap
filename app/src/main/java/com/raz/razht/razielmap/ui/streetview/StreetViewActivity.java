package com.raz.razht.razielmap.ui.streetview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.raz.razht.razielmap.R;

/**
 * This class for get the "streetView" pnorma view from Google Maps API
 *
 * @author Raziel Shushan
 */
public class StreetViewActivity extends AppCompatActivity {
    private LatLng latLng;
    private StreetViewPanorama mStreetViewPanorama;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_street_view );
        Intent intent = getIntent();
        latLng = new LatLng( Double.parseDouble( intent.getStringExtra( "LAT" ) ), Double.parseDouble( intent.getStringExtra( "LANG" ) ) );
        SupportStreetViewPanoramaFragment streetViewPanoramaFragment =
                (SupportStreetViewPanoramaFragment)
                        getSupportFragmentManager().findFragmentById( R.id.streetviewpanorama );
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                new OnStreetViewPanoramaReadyCallback() {
                    @Override
                    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
                        // loaded which is when the savedInstanceState is null).
                        if (savedInstanceState == null) {
                            panorama.setStreetNamesEnabled( true );
                            panorama.setUserNavigationEnabled( true );
                            panorama.setZoomGesturesEnabled( true );
                            panorama.setPanningGesturesEnabled( true );
                            panorama.setPosition( latLng );
                        }
                    }
                } );
    }
}


