package com.raz.razht.razielmap.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {


    /**
     * This is a helper class for create the database in the application.
     *
     * @author Raziel Shushan
     */
    private static final String CREATE_TABLE_FAVORITE = "CREATE TABLE " + DBConstants.TABLE_NAME_FAVORITE + " (" + DBConstants.COLUMN_NAME_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DBConstants.COLUMN_NAME_LOCATION_NAME + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_ICON + " TEXT, " +
            DBConstants.COLUMN_NAME_LOCATION_ADDRESS + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE + " TEXT, "
            + DBConstants.COLUMN_NAME_API_ID + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_RATING + " REAL," + DBConstants.COLUMN_NAME_LOCATION_IMAGE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_IS_OPEN + " INTEGER );";

    private static final String CREATE_TABLE_NEARBY_RESTAURANT = "CREATE TABLE " + DBConstants.TABLE_NAME_NEARBY_RESTAURANT + " (" + DBConstants.COLUMN_NAME_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DBConstants.COLUMN_NAME_LOCATION_NAME + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_ICON + " TEXT, " +
            DBConstants.COLUMN_NAME_LOCATION_ADDRESS + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE + " TEXT, "
            + DBConstants.COLUMN_NAME_API_ID + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_RATING + " REAL," + DBConstants.COLUMN_NAME_LOCATION_IMAGE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_IS_OPEN + " INTEGER );";

    private static final String CREATE_TABLE_NEARBY_BAR = "CREATE TABLE " + DBConstants.TABLE_NAME_NEARBY_BAR + " (" + DBConstants.COLUMN_NAME_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DBConstants.COLUMN_NAME_LOCATION_NAME + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_ICON + " TEXT, " +
            DBConstants.COLUMN_NAME_LOCATION_ADDRESS + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE + " TEXT, "
            + DBConstants.COLUMN_NAME_API_ID + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_RATING + " REAL," + DBConstants.COLUMN_NAME_LOCATION_IMAGE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_IS_OPEN + " INTEGER );";

    private static final String CREATE_TABLE_NEARBY_CAFE = "CREATE TABLE " + DBConstants.TABLE_NAME_NEARBY_CAFE + " (" + DBConstants.COLUMN_NAME_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DBConstants.COLUMN_NAME_LOCATION_NAME + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_ICON + " TEXT, " +
            DBConstants.COLUMN_NAME_LOCATION_ADDRESS + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE + " TEXT, "
            + DBConstants.COLUMN_NAME_API_ID + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_RATING + " REAL," + DBConstants.COLUMN_NAME_LOCATION_IMAGE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_IS_OPEN + " INTEGER );";

    private static final String CREATE_TABLE_NEARBY_HOTEL = "CREATE TABLE " + DBConstants.TABLE_NAME_NEARBY_HOTEL + " (" + DBConstants.COLUMN_NAME_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DBConstants.COLUMN_NAME_LOCATION_NAME + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_ICON + " TEXT, " +
            DBConstants.COLUMN_NAME_LOCATION_ADDRESS + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE + " TEXT, "
            + DBConstants.COLUMN_NAME_API_ID + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_RATING + " REAL," + DBConstants.COLUMN_NAME_LOCATION_IMAGE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_IS_OPEN + " INTEGER );";

    private static final String CREATE_TABLE_NEARBY_GAS_STATION = "CREATE TABLE " + DBConstants.TABLE_NAME_NEARBY_GAS_STATION + " (" + DBConstants.COLUMN_NAME_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DBConstants.COLUMN_NAME_LOCATION_NAME + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_ICON + " TEXT, " +
            DBConstants.COLUMN_NAME_LOCATION_ADDRESS + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE + " TEXT, "
            + DBConstants.COLUMN_NAME_API_ID + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_RATING + " REAL," + DBConstants.COLUMN_NAME_LOCATION_IMAGE + " TEXT, " + DBConstants.COLUMN_NAME_LOCATION_IS_OPEN + " INTEGER );";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super( context, DBConstants.DATABASE_NAME, null, 1 );

    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        try {

            db.execSQL( CREATE_TABLE_FAVORITE );
            db.execSQL( CREATE_TABLE_NEARBY_RESTAURANT );
            db.execSQL( CREATE_TABLE_NEARBY_BAR );
            db.execSQL( CREATE_TABLE_NEARBY_CAFE );
            db.execSQL( CREATE_TABLE_NEARBY_HOTEL );
            db.execSQL( CREATE_TABLE_NEARBY_GAS_STATION );


        } catch (SQLiteException e) {
            e.getCause();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL( "DROP TABLE IF EXISTS " + DBConstants.TABLE_NAME_FAVORITE );
        db.execSQL( "DROP TABLE IF EXISTS " + DBConstants.TABLE_NAME_NEARBY_RESTAURANT );
    }
}
