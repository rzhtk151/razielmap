package com.raz.razht.razielmap.room;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.raz.razht.razielmap.App;

import java.util.List;

public class PlaceRepository {
    private PlaceDao mPlaceDao;
    private LiveData<List<PlaceRoom>> mAllNearbyPlaces;
    private LiveData<List<PlaceRoom>> getAllFavoritePlaces;

    App appReference;

    public PlaceRepository(Application application) {
        PlaceRoomDataBase db = PlaceRoomDataBase.getInstance( application );
        appReference = (App) application;
        this.mPlaceDao = db.placeDao();
        this.mAllNearbyPlaces = mPlaceDao.getAllPlaces( appReference.getPreferenceHelper().getTypeModePref() );
        this.getAllFavoritePlaces = mPlaceDao.getAllFavoritePlaces();
    }

    public LiveData<List<PlaceRoom>> getmAllNearbyPlaces() {
        return mAllNearbyPlaces;
    }

    public LiveData<List<PlaceRoom>> getGetAllFavoritePlaces() {
        return getAllFavoritePlaces;
    }


    public void insert(PlaceRoom place) {
        new insertPlaceAsyncTask( mPlaceDao ).execute( place );
    }

    class insertPlaceAsyncTask extends AsyncTask<PlaceRoom, Void, Void> {

        private PlaceDao mAsyncTaskDao;

        public insertPlaceAsyncTask(PlaceDao mUserDao) {
            this.mAsyncTaskDao = mUserDao;
        }

        @Override
        protected Void doInBackground(PlaceRoom... place) {
            mAsyncTaskDao.insert( place[0] );
            return null;
        }
    }


}
