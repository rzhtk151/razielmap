package com.raz.razht.razielmap.db;

/**
 * This is a class to store the text constants for App Db
 *
 * @author Raziel Shushan
 */


public class DBConstants {
    public final static int DATABASE_VERSION = 1;
    public final static String DATABASE_NAME = "map.db";
    public final static String COLUMN_NAME_id = "_id";
    public final static String TABLE_NAME_FAVORITE = "table_raziel_map_favorite";
    public final static String TABLE_NAME_NEARBY_RESTAURANT = "table_restaurant";
    public final static String TABLE_NAME_NEARBY_BAR = "table_BAR";
    public final static String TABLE_NAME_NEARBY_CAFE = "table_CAFE";
    public final static String TABLE_NAME_NEARBY_HOTEL = "table_HOTEL";
    public final static String TABLE_NAME_NEARBY_GAS_STATION = "table_GAS_STATION";
    public final static String COLUMN_NAME_LOCATION_NAME = "locationName";
    public final static String COLUMN_NAME_LOCATION_ADDRESS = "address";
    public final static String COLUMN_NAME_LOCATION_ICON = "icon";
    public final static String COLUMN_NAME_LOCATION_RATING = "rating";
    public final static String COLUMN_NAME_LOCATION_IMAGE = "image";
    public final static String COLUMN_NAME_LOCATION_LOCATION_TYPE = "type";
    public final static String COLUMN_NAME_LOCATION_LOCATION_LONGITUDE = "longitude";
    public final static String COLUMN_NAME_LOCATION_LOCATION_LATITUDE = "latitude";
    public final static String COLUMN_NAME_LOCATION_LOCATION_DISTANCE = "distance";
    public final static String COLUMN_NAME_API_ID = "api_ID";
    public final static String COLUMN_NAME_IS_FAVORITE = "is_favorite";


    //1 = true, 0 = false
    public final static String COLUMN_NAME_LOCATION_IS_OPEN = "isOpen";


}
