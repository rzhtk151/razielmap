package com.raz.razht.razielmap.ui.place;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This activity shows the image selection from PlaceActivity imageView recyclerView
 *
 * @author Raziel Shushahn
 */
public class FullScreenImageActivity extends AppCompatActivity {
    //ui object
    @BindView(R.id.full_screen_image)
    ImageView imageView;

    //app reference
    App appReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_full_screen_image );
        ButterKnife.bind( this );
        appReference = (App) getApplicationContext();
        Intent callingActivityIntent = getIntent();
        if (callingActivityIntent != null) {
            String imagePath = callingActivityIntent.getStringExtra( Constants.EXTRA_PATH_IMAGE );
            if (imagePath != null && imageView != null) {
                appReference.getFile().loadImageFromStorage( imagePath, imageView );
            }
        }
    }
}
