package com.raz.razht.razielmap.ui.about;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class AboutPageActivity extends AppCompatActivity {

    @BindView(R.id.versionButton)
    Button versionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_about_page );
        ButterKnife.bind( this );
        versionButton.append( " " + getString( R.string.versionName ) );
    }

    private Intent buildIntent(String intentAction, String destinationUri) {
        Intent intent = new Intent( intentAction );
        Uri data = Uri.parse( destinationUri );
        intent.setData( data );
        return intent;
    }

    @OnClick(R.id.sourceCodeButton)
    public void openSourceCode() {
        Intent openSourceCodeWebPage = buildIntent( Intent.ACTION_VIEW, Constants.URL_SOURCE_CODE );
        startActivity( openSourceCodeWebPage );
    }

    @OnClick(R.id.linkedinButton)
    public void openLinkedinUser() {
        Intent openLinkedinUser = buildIntent( Intent.ACTION_VIEW, Constants.URL_LINKEDIN );
        startActivity( openLinkedinUser );
    }

    @OnClick(R.id.rateButton)
    public void openStorePage() {
        try {
            startStorePageInPlayStoreApp();
        } catch (ActivityNotFoundException e) {
            startStorePageInBrowser();
        }
    }

    @OnClick(R.id.razielMovieApp)
    public void openRazielMovieApp() {
        Intent openRazielMovieApp = buildIntent( Intent.ACTION_VIEW, Constants.URL_STORE_RAZIEL_MOVIE_APP );
        startActivity( openRazielMovieApp );
    }


    private void startStorePageInPlayStoreApp() {
        Intent startPlayStorePage = buildIntent( Intent.ACTION_VIEW, Constants.URL_STORE );
        startActivity( startPlayStorePage );
    }

    private void startStorePageInBrowser() {
        Intent startPlayStoreWebPage = buildIntent( Intent.ACTION_VIEW, Constants.URL_STORE_BACKUP );
        startActivity( startPlayStoreWebPage );
    }

    @OnClick(R.id.emailButton)
    public void openEmailIntent() {
        Intent emailToAuthor = buildIntent( Intent.ACTION_SENDTO, Constants.URL_EMAIL );
        Intent chooser = Intent.createChooser( emailToAuthor, getString( R.string.send_mail_using ) );
        startActivity( chooser );
    }
}
