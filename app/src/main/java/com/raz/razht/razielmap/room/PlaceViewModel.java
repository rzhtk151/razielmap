package com.raz.razht.razielmap.room;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class PlaceViewModel extends AndroidViewModel {
    private PlaceRepository mPlaceRepository;

    private LiveData<List<PlaceRoom>> mAllNearByPlaces;
    private LiveData<List<PlaceRoom>> mAllFavoritePlaces;


    public PlaceViewModel(@NonNull Application application) {
        super( application );
        mPlaceRepository = new PlaceRepository( application );
        mAllNearByPlaces = mPlaceRepository.getmAllNearbyPlaces();
        mAllFavoritePlaces = mPlaceRepository.getGetAllFavoritePlaces();
    }

    public LiveData<List<PlaceRoom>> getmAllNearByPlaces() {
        return mAllNearByPlaces;
    }

    public LiveData<List<PlaceRoom>> getmAllFavoritePlaces() {
        return mAllFavoritePlaces;
    }

    public void insert(PlaceRoom place) {
        mPlaceRepository.insert( place );
    }
}
