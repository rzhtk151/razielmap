package com.raz.razht.razielmap;

import android.app.Application;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatDelegate;
import android.util.DisplayMetrics;

import com.raz.razht.razielmap.data.preference.PreferenceHelper;
import com.raz.razht.razielmap.db.DBHandler;
import com.raz.razht.razielmap.db.FileHelper;

import java.util.Locale;

/**
 * Using this class allows casting it to getApplication() in Android classes that implement Context.
 * This way the properties that it contains are available in the entire application and do not need
 * to be re-instantiated elsewhere.
 *
 * @author Raziel Shushahn
 */
public final class App extends Application {
    private DBHandler handler;
    private PreferenceHelper preferenceHelper;
    private FileHelper file;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new DBHandler( this );
        preferenceHelper = new PreferenceHelper( this );
        PreferenceHelper.initialize( this );
        file = new FileHelper( this );
        setLang();
        AppCompatDelegate.setDefaultNightMode( preferenceHelper.getTheme() );
    }

    /**
     * @return the DBHelper  instance
     */
    public DBHandler getHandler() {
        return handler;
    }

    /**
     * @return the PreferenceHelper  instance
     */
    public PreferenceHelper getPreferenceHelper() {
        return preferenceHelper;
    }

    /**
     * @return the FileHelper instance
     */
    public FileHelper getFile() {
        return file;
    }

    public void setLang() {
        String lang = preferenceHelper.getPreferenceLanguageApp();
        Locale local = new Locale( lang );
        setLocale( local );
    }

    public void setLocale(Locale locale) {
        Locale.setDefault( locale );
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration( conf, dm );
    }
}
