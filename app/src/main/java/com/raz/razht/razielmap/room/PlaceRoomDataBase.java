package com.raz.razht.razielmap.room;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

@Database(entities = {PlaceRoom.class}, version = 1, exportSchema = false)
public abstract class PlaceRoomDataBase extends RoomDatabase {
    private static PlaceRoomDataBase INSTANCE;

    public abstract PlaceDao placeDao();

    private static final String DB_NAME = "places.db";

    public static PlaceRoomDataBase getInstance(@NonNull final Context context) {
        synchronized (PlaceRoomDataBase.class) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder( context.getApplicationContext(),
                        PlaceRoomDataBase.class, DB_NAME )
                        .build();

            }
        }
        return INSTANCE;
    }


}
