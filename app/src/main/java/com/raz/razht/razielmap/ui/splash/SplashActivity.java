package com.raz.razht.razielmap.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.ui.activity.main.MainActivity;

/**
 * This is the splash screen of the application
 * its the first activity that runs
 * the activity contain hello text and app icon
 *
 * @author Raziel Shushan
 */
public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.splash_activty );

        new Handler().postDelayed( new Runnable() {
            @Override
            public void run() {
                Intent mainActivity = new Intent( SplashActivity.this, MainActivity.class );
                startActivity( mainActivity );
                finish();
            }
        }, SPLASH_TIME_OUT );

    }
}
