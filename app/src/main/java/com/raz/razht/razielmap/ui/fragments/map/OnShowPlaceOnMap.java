package com.raz.razht.razielmap.ui.fragments.map;

import com.raz.razht.razielmap.db.Place;

public interface OnShowPlaceOnMap {
    void onPlaceSelectedForMap(Place placeId);
}
