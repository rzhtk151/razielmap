package com.raz.razht.razielmap.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;

/**
 * This class create for dealing with the DB App throughout the application
 *
 * @author Raziel Shushan
 */
public class DBHandler {

    //objects
    private DBHelper helper;

    public DBHandler(Context context) {
        this.helper = new DBHelper( context, DBConstants.DATABASE_NAME, null, DBConstants.DATABASE_VERSION );

    }

    /**
     * @param tableName called to know the table for update
     * @param places    it's arrayList with places - the db to storage
     * @this method created to insert full all db storage in one call(max 20), the function called after the app start to update is DB Type storage
     * there is 5 relevant table for this function - restaurant, coffee, bar, hotels, gas station.
     * the function delete all relevant table storage at the beginning
     */
    public void insertArrayPlace(ArrayList<Place> places, String tableName) {
        deleteAllPlaces( selectAllPlaces( tableName ), tableName );
        SQLiteDatabase database = helper.getWritableDatabase();
        try {
            for (int i = 0; i < places.size(); i++) {
                ContentValues values = new ContentValues();
                values.put( DBConstants.COLUMN_NAME_LOCATION_NAME, places.get( i ).getLocationName() );
                values.put( DBConstants.COLUMN_NAME_LOCATION_ADDRESS, places.get( i ).getAddress() );
                values.put( DBConstants.COLUMN_NAME_API_ID, places.get( i ).getApiId() );
                values.put( DBConstants.COLUMN_NAME_LOCATION_ICON, places.get( i ).getIcon() );
                values.put( DBConstants.COLUMN_NAME_LOCATION_IMAGE, places.get( i ).getImagePlace() );
                values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE, places.get( i ).getLang() );
                values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE, places.get( i ).getLat() );
                values.put( DBConstants.COLUMN_NAME_LOCATION_RATING, places.get( i ).getRating() );
                values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE, places.get( i ).getType() );
                values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE, places.get( i ).getDistance() );
                values.put( DBConstants.COLUMN_NAME_LOCATION_IS_OPEN, places.get( i ).getIsOpen() );
                database.insert( tableName, null, values );
            }
        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
    }

    /**
     * this function can called from:
     * 1 - placeActivity - after the user click on favorite icon
     * 2 - ListFragment - after the user use onLongClickListener and tap
     *
     * @param place     The user call this function after he insert place to is favorite table list
     * @param tableName to know which table add the place
     * @return boolean value If it succeeded or failed
     */
    public boolean insertPlace(Place place, String tableName) {
        SQLiteDatabase database = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put( DBConstants.COLUMN_NAME_LOCATION_NAME, place.getLocationName() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_ADDRESS, place.getAddress() );
            values.put( DBConstants.COLUMN_NAME_API_ID, place.getApiId() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_ICON, place.getIcon() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_IMAGE, place.getImagePlace() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE, place.getLang() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE, place.getLat() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_RATING, place.getRating() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE, place.getType() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE, place.getDistance() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_IS_OPEN, place.getIsOpen() );
            database.insert( tableName, null, values );

        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
        return true;
    }

    //Update existing Place data in DB
    public boolean updatePlace(Place place, String tableName) {
        SQLiteDatabase database = helper.getWritableDatabase();

        try {
            ContentValues values = new ContentValues();
            values.put( DBConstants.COLUMN_NAME_LOCATION_NAME, place.getLocationName() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_ADDRESS, place.getAddress() );
            values.put( DBConstants.COLUMN_NAME_API_ID, place.getApiId() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_ICON, place.getIcon() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_IMAGE, place.getImagePlace() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE, place.getLang() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE, place.getLat() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_RATING, place.getRating() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE, place.getType() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE, place.getDistance() );
            values.put( DBConstants.COLUMN_NAME_LOCATION_IS_OPEN, place.getIsOpen() );
            database.update( tableName, values, "_id=?", new String[]{String.valueOf( place.getId() )} );
        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
        return true;
    }

    /**
     * Deletes a place from the database as well as deletes
     *
     * @param apiId The place api id to delete.
     */
    public boolean deletePlace(String apiId, String tableName) {
        SQLiteDatabase database = helper.getWritableDatabase();

        try {
            database.delete( tableName, DBConstants.COLUMN_NAME_API_ID + "=?", new String[]{apiId} );

        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
        return true;
    }

    /**
     * Deletes arrayList of place from the database
     * The function can be called twice:
     * 1 - preference setting to delete all favorite storage
     * 2 - DB handler - from "insertArrayPlace" function(called before add the places)
     *
     * @param place     The arrayList of places to add
     * @param tableName to know which table delete the places
     */

    public boolean deleteAllPlaces(ArrayList<Place> place, String tableName) {
        SQLiteDatabase database = helper.getWritableDatabase();

        try {
            for (int i = 0; i < place.size(); i++) {
                database.delete( tableName, "_id=?", new String[]{String.valueOf( place.get( i ).getId() )} );
            }

        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
        return true;
    }


    /**
     * function called to check if the place is on the favorite Db
     * Used before the user add new place
     *
     * @param apiId The api ID of the place in the local DB.
     * @return true if exits or false if not.
     */
    public boolean checkIfPlacesExitsInMyFavoriteDB(String apiId) {
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = null;

        try {
            cursor = database.query( DBConstants.TABLE_NAME_FAVORITE, null, DBConstants.COLUMN_NAME_API_ID + "=?", new String[]{apiId}, null, null, null );
        } catch (SQLiteException e) {
            e.getCause();
        }
        if (!(cursor.moveToFirst()) || cursor.getCount() == 0)
            return false;
        else
            return true;
    }

    /**
     * function called to select one place from DB
     * Used one time - whe n the user get inside the placeActivity
     *
     * @param api_Id    The api ID of the place in the local DB.
     * @param tableName The table name to find the place
     * @return The place object.
     */
    public Place selectPlaceByPlaceApiId(String api_Id, String tableName) {
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = null;
        //String query = "SELECT " + DBConstants.COLUMN_NAME_API_ID + " FROM " + tableName + " WHERE " + DBConstants.COLUMN_NAME_API_ID + "=?";
        // String query = "SELECT ALL FROM " + tableName + " WHERE " + DBConstants.COLUMN_NAME_API_ID + "=?";

        //String query = "SELECT * FROM " + tableName +  " WHERE " + DBConstants.COLUMN_NAME_API_ID + " = " + api_Id;

        try {
            cursor = database.query( tableName, null, DBConstants.COLUMN_NAME_API_ID + "=?", new String[]{api_Id}, null, null, null );

        } catch (SQLiteException e) {
            e.getCause();
        }
        if (cursor != null)
            cursor.moveToFirst();
        Place place;

        String apiId = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_API_ID ) );
        int id = cursor.getInt( cursor.getColumnIndex( DBConstants.COLUMN_NAME_id ) );
        String name = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_NAME ) );
        String address = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_ADDRESS ) );
        String icon = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_ICON ) );
        String image = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_IMAGE ) );
        double rating = cursor.getDouble( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_RATING ) );
        String type = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE ) );
        String lang = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE ) );
        String lat = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE ) );
        String distance = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE ) );
        int isOpen = cursor.getInt( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_IS_OPEN ) );
        place = new Place( id, apiId, name, address, icon, image, rating, type, lang, lat, distance, isOpen );
        return place;
    }

    /**
     * function called to find place api_id by place name
     * Used only in "mapFragment" after the user click on map marker
     *
     * @param name      The name of the place in the local DB.
     * @param tableName The table name to find the place
     * @return String api_id
     */
    public String getPlaceIdByName(String name, String tableName) {
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = null;

        try {
            cursor = database.query( tableName, null, DBConstants.COLUMN_NAME_LOCATION_NAME + "=?", new String[]{name}, null, null, null );

        } catch (SQLiteException e) {
            e.getCause();
        }
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            String place_apiId = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_API_ID ) );

            return place_apiId;
        } else {
            return null;
        }

    }

    /**
     * function called to select all table places from DB
     *
     * @param tableName The table name to get specific table
     * @return ArrayList of places
     */
    public ArrayList<Place> selectAllPlaces(String tableName) {
        SQLiteDatabase database = helper.getReadableDatabase();
        Cursor cursor = null;

        try {
            cursor = database.query( tableName, null, null, null, null, null, null );
        } catch (SQLiteException e) {
            e.getCause();
        }
        ArrayList<Place> table = new ArrayList<>();

        while (cursor.moveToNext()) {

            int id = cursor.getInt( cursor.getColumnIndex( DBConstants.COLUMN_NAME_id ) );
            String apiId = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_API_ID ) );
            String name = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_NAME ) );
            String address = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_ADDRESS ) );
            String icon = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_ICON ) );
            String image = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_IMAGE ) );
            double rating = cursor.getDouble( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_RATING ) );
            String type = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_LOCATION_TYPE ) );
            String lang = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LONGITUDE ) );
            String lat = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_LOCATION_LATITUDE ) );
            String distance = cursor.getString( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_LOCATION_DISTANCE ) );
            int isOpen = cursor.getInt( cursor.getColumnIndex( DBConstants.COLUMN_NAME_LOCATION_IS_OPEN ) );


            table.add( new Place( id, apiId, name, address, icon, image, rating, type, lang, lat, distance, isOpen ) );
        }

        cursor.close();

        return table;
    }


}

