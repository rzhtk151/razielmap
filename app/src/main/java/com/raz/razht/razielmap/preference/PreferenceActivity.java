package com.raz.razht.razielmap.preference;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.DisplayMetrics;

import com.raz.razht.razielmap.App;
import com.raz.razht.razielmap.R;
import com.raz.razht.razielmap.data.preference.PreferenceHelper;
import com.raz.razht.razielmap.db.DBConstants;
import com.raz.razht.razielmap.ui.activity.main.MainActivity;
import com.raz.razht.razielmap.ui.dialog.SimpleDialog;

import java.util.Locale;

/**
 * This is the "Settings" activity.
 * Its use is changing the default SharedPreferences in the application
 *
 * @author Raziel Shushan
 * @see SharedPreferences
 * @see PreferenceHelper
 */

public class PreferenceActivity extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    public boolean firstTime = false;
    private App appReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        appReference = (App) getApplication();

        getFragmentManager().beginTransaction().replace( android.R.id.content, new MyPreferenceFragment() ).commit();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences( this );
        preferences.registerOnSharedPreferenceChangeListener( this );


    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case PreferenceHelper.PREFERENCE_NIGHT_MODE_NAME:
                if (sharedPreferences.getBoolean( key, false ))
                    AppCompatDelegate.setDefaultNightMode( AppCompatDelegate.MODE_NIGHT_YES );
                else
                    AppCompatDelegate.setDefaultNightMode( AppCompatDelegate.MODE_NIGHT_NO );

                this.recreate();
                break;
            case PreferenceHelper.PREFERENCE_LANGUAGE_APP_NAME:
                String lang = sharedPreferences.getString( PreferenceHelper.PREFERENCE_LANGUAGE_APP_NAME, "en" );
                Locale local = new Locale( lang );
                setLocale( local );
                this.recreate();
                //restartActivity();
                break;
            case PreferenceHelper.PREFERENCE_UNITS_APP_NAME:
                sharedPreferences.edit().putString( PreferenceHelper.PREFERENCE_UNITS_APP_NAME, PreferenceHelper.PREFERENCE_UNITS_APP_NAME );
                this.recreate();
                break;
            case PreferenceHelper.PREFERENCE_DELETE_FAVORITES_NAME:
                AlertDialog.Builder deleteAllFavorite = new AlertDialog.Builder( getApplicationContext() );
                deleteAllFavorite.setMessage( R.string.delete_all_your_favorite_storage );
                SimpleDialog simpleDialog = new SimpleDialog();
                simpleDialog.show( getSupportFragmentManager(), "g" );
                deleteAllFavorite.setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        appReference.getHandler().deleteAllPlaces( appReference.getHandler().selectAllPlaces( DBConstants.TABLE_NAME_FAVORITE ), DBConstants.TABLE_NAME_FAVORITE );
                    }
                } );
                break;

            case PreferenceHelper.PREFERENCE_RADIUS_APP_NAME:
                appReference.getPreferenceHelper().setChangePreferenceRadius( true );
                break;
        }
    }

    /**
     * Used to set the app locale
     */
    public void setLocale(Locale locale) {
        Locale.setDefault( locale );
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration( conf, dm );
    }

    /**
     * This function restart the app - it happen after the user press on the backButton
     */
    private void restartActivity() {
        Intent intent = new Intent( this, MainActivity.class );
        finish();
        startActivity( intent );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        restartActivity();
    }

    public static class MyPreferenceFragment extends android.preference.PreferenceFragment {

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate( savedInstanceState );
            addPreferencesFromResource( R.xml.preference_setting );
        }

    }
}
