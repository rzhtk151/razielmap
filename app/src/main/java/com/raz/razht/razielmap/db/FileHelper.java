package com.raz.razht.razielmap.db;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * This is a helper class for dealing with the Sane Images to App phone Storage
 *
 * @author Raziel Shushan
 */
public class FileHelper {
    private Context context;

    public FileHelper(Context context) {
        this.context = context;
    }

    /**
     * This funcatin save the image to phone storage
     *
     * @param fileName - file name to save
     * @param image    - the image in Bitnap format to save
     */
    public String saveImage(Bitmap image, String fileName) {
        String savedImagePath = null;

        String imageFileName = fileName + ".jpg";
        File storageDir = new File( context.getFilesDir() + "/IMAGE_RAZIEL_MAP" );
        boolean success = true;
        if (!storageDir.exists()) {
            success = storageDir.mkdirs();
        }
        if (success) {
            File imageFile = new File( storageDir, imageFileName );
            savedImagePath = imageFile.getAbsolutePath();
            try {
                OutputStream fOut = new FileOutputStream( imageFile );
                image.compress( Bitmap.CompressFormat.JPEG, 100, fOut );
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return savedImagePath;
    }


    /**
     * A function that loads the image from the DB
     */
    public void loadImageFromStorage(String path, ImageView imageView) {

        try {
            File f = new File( path );
            Bitmap b = BitmapFactory.decodeStream( new FileInputStream( f ) );
            imageView.setImageBitmap( b );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


}
