package com.raz.razht.razielmap.Service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.raz.razht.razielmap.util.Constants;
import com.raz.razht.razielmap.util.DownloadUrl;

/**
 * This is The class to send the query for google APi
 * every method send diffren query
 * Details are sent to the class that registers to this broadcast Service
 *
 * @author Raziel Shushan
 */
public class GoogleApiService extends IntentService {
    //constants
    public final static String BROADCAST_FOR_SERVICE_FINISHED_RESPONSE_NEARBY_PLACES = "com.example.razht.razielmap.Service.nearbyplaces";
    public final static String BROADCAST_FOR_SERVICE_FINISHED_RESPONSE_API_ID = "com.example.razht.razielmap.Service.placesapiid";

    public GoogleApiService() {
        super( "" );
    }

    public static void onSendPlaceLiveData(Context context, String str) {
        Intent intent = new Intent( context, GoogleApiService.class );
        intent.putExtra( "URL", str );
        intent.putExtra( "PLACE_RECEIVER", true );
        intent.putExtra( Constants.FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY, "LIVE_DATA" );
        context.startService( intent );
    }

    public static void onNearByPlacesQueryCreateAppDB(Context context, String str, String who, String type) {
        Intent intent = new Intent( context, GoogleApiService.class );
        intent.putExtra( "URL", str );
        intent.putExtra( "TYPE", type );
        intent.putExtra( Constants.FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY, who );
        context.startService( intent );
    }

    public static void onSendImagesByPlaceIdQuery(Context context, String str) {
        Intent intent = new Intent( context, GoogleApiService.class );
        intent.putExtra( "URL", str );
        intent.putExtra( "PLACE_RECEIVER", true );
        intent.putExtra( Constants.FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY, Constants.FRAGMENT_PLACES_NAME_IMAGES_QUERY );
        context.startService( intent );
    }

    public static void onSendQueryByPlaceId(Context context, String str) {
        Intent intent = new Intent( context, GoogleApiService.class );
        intent.putExtra( "PLACE_RECEIVER", true );
        intent.putExtra( "URL", str );
        intent.putExtra( Constants.FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY, Constants.FRAGMENT_PLACES_NAME_PLACE_ID_QUERY );
        context.startService( intent );
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String url = intent.getStringExtra( "URL" );
        String receive = DownloadUrl.sendHttpRequest( url );
        Intent broadcast;
        if (intent.getBooleanExtra( "PLACE_RECEIVER", false )) {
            broadcast = new Intent( BROADCAST_FOR_SERVICE_FINISHED_RESPONSE_API_ID );
        } else {
            broadcast = new Intent( BROADCAST_FOR_SERVICE_FINISHED_RESPONSE_NEARBY_PLACES );
        }
        broadcast.putExtra( "TYPE", intent.getStringExtra( "TYPE" ) );
        String token = intent.getStringExtra( "TOKEN" );
        if (token != null)
            broadcast.putExtra( "TOKEN", true );

        broadcast.putExtra( "FromService", receive );
        broadcast.putExtra( Constants.FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY, intent.getStringExtra( Constants.FROM_WHERE_THE_FRAGMENT_COME_TO_MAIN_ACTIVITY ) );
        LocalBroadcastManager.getInstance( this ).sendBroadcast( broadcast );
    }


}
