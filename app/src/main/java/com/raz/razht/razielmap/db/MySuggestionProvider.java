package com.raz.razht.razielmap.db;

import android.content.SearchRecentSuggestionsProvider;


public class MySuggestionProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.example.razht.razielmap.db.MySuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES | DATABASE_MODE_2LINES;

    public MySuggestionProvider() {
        setupSuggestions( AUTHORITY, MODE );
    }

}
